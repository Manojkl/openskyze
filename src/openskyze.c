#include <stdio.h>
#include <glib.h>

#include <openskyze/openskyze.h>

int main (int argc, char *argv[]) 
{
	int rc = SKYZE_SUCCESS;
	GOptionContext *ctx = NULL;
	GOptionGroup *group = NULL;
	GError *error = NULL;
	char *cfg_file = NULL;
	skyze_app_t *app = NULL;

	GOptionEntry entries[] = {
		{"config-file", 'c', 0, G_OPTION_ARG_FILENAME, &cfg_file,
			"Specify the yaml file", NULL},
		{NULL},
	};

	ctx = g_option_context_new("openskyze");
	group = g_option_group_new("abc", NULL, NULL, NULL, NULL);
	g_option_group_add_entries(group, entries);
	g_option_context_set_main_group(ctx, group);

	if (!g_option_context_parse(ctx, &argc, &argv, &error)) {
		g_print ("%s",g_option_context_get_help (ctx, TRUE, NULL)); 
		rc = -1;
		goto exit;
	}

	skyze_init();

	if (cfg_file) {
		app = skyze_app_new_with_config(cfg_file);

		if (app == NULL) {
			g_print("Error failed to load app with cfg '%s'\n", cfg_file);
			rc = -1;
			goto exit;
		}

		skyze_app_run(app);
	}
exit:
	if (app) {
		skyze_app_free(app);
	}

	skyze_deinit();
	g_option_context_free(ctx);
	g_option_group_unref(group);

    return rc;
}
