# Design

OpenSkyze is built upon the [NVIDIA Deepstream SDK](https://developer.nvidia.com/deepstream-sdk).  The Deepstream SDK provides a powerful framework to enable real-time analytics and video processing and OpenSkyze will built an easily deployable platform to allow users to quickly configure and deploy capabilities into the cloud and on edge devices.  The goal of our proposed to design is to make the platform configurable, extensivle, and easy to deploy.

The library will be developed using C and will maximize the use of GLib since Deepstream is implemented using GStreamer, which already makes use of GLib.  We also plan on providing bindings for popular languages such as python and golang.  We would also be happy to receive contributions that add support for additional bindings.


## Configuration layout / objects
The configuration code is functionand can be reviewed in libskyze_config.c.  The layout and design is under heavy development and is subject to change.

The OpenSkyze configuration is broken down into the following objects:

* Config - YAML-based configuration files using [libcyaml](https://github.com/tlsa/libcyaml).  The configuration objects are broken down as follows:
  * app
  * source
  * sink
  * inference (primary/secondary gie)
  * osd
  * streammux
  * primary-gie
  * tracker
  * sinkX
  * sourceX

* source - Abstraction of Video sources. Currently this list includes:
   * CameraV4L2
   * URI

* sink
  * FakeSink 
  * EglSink 
  * UDPSink 
  * nvoverlaysink 
  * MsgConvBroker

* Protocol Adapter - Implement protocol adapter using [NvMsgBroker API](https://docs.nvidia.com/metropolis/deepstream/dev-guide/text/DS_plugin_gst-nvmsgbroker.html) based on [librabbitmq-c](https://github.com/alanxz/rabbitmq-c).  Instead of JSON, nanopb (protobuf) will be used for message format.

## API Examples

We are working towards a few prototypes and milestones to build out the initial openskyze platform:


Milestone #1: Create an App with a single source and a single sink.  (In progress)
```
#include <libskyze_app.h>

int main(int argc, char *argv[])
{
	int rc = SKYZE_SUCCESS;
	skyze_app_t *app;
	skyze_source_t *source;
	skyze_sink_t *sink;
	skyze_credentials_t creds = {0};

	creds.username = "test";
	creds.password = "test123";

	app = skyze_app_new();
	source = skyze_source_uri_new("source_name", 0, 0, "rtsp://192.168.1.108:554", &creds, NULL);
	sink = skyze_sink_rtsp_new("sink_name", "*:554", "username", "password123", NULL);

	skyze_app_source_add(app, source); // read video stream from rtsp device
	skyze_app_sink_add(app, sink); // output of video stream will be server via rtsp server

	skyze_app_run(app); // runs pipeline, blocks forever

	return rc;
}

```

Milestone #2: Create an App with a single source and a single sink, an an inference engine.  (TODO)
```
#include <libskyze_app.h>

int main(int argc, char *argv[])
{
	int rc = SKYZE_SUCCESS;
	skyze_app_t *app;
	skyze_source_t *source;
	skyze_sink_t *sink;
	skyze_credentials_t creds = {0};

	creds.username = "test";
	creds.password = "test123";

	app = skyze_app_new();
	source = skyze_source_uri_new("source_name", 0, 0, "rtsp://192.168.1.108:554", &creds, NULL);
	sink = skyze_sink_rtsp_new("sink_name", "*:554", "username", "password123", NULL);
	primary_gie = skyze_inference_new("name", SKYZE_INFERENCE_MODE_PRIMARY, SKYZE_INFERENCE_MODEL_YOLOV3);

	skyze_app_source_add(app, source); // read video stream from rtsp device
	skyze_app_sink_add(app, sink); // output of video stream will be server via rtsp server
	skyze_app_inference_add(app, primary_gie); // run video frames through YOLOV3

	skyze_app_run(app); // runs pipeline, blocks forever

	return rc;
}

```

Milestone #3: Create an App with a single source and a single sink, an an inference engine, and a message publisher.  (TODO)
```
#include <libskyze_app.h>

int main(int argc, char *argv[])
{
	int rc = SKYZE_SUCCESS;
	skyze_app_t *app;
	skyze_source_t *source;
	skyze_sink_t *sink;
	skyze_credentials_t creds = {0};
	skyze_cloudmsg_t *cloudmsg;

	creds.username = "test";
	creds.password = "test123";

	app = skyze_app_new();
	source = skyze_source_uri_new("source_name", 0, 0, "rtsp://192.168.1.108:554", &creds, NULL); 
	sink = skyze_sink_rtsp_new("sink_name", "*:554", "username", "password123", NULL);
	primary_gie = skyze_inference_new("name", SKYZE_INFERENCE_MODE_PRIMARY, SKYZE_INFERENCE_MODEL_YOLOV3);
	cloudmsg = skyze_cloudmsg_new("name", SKYZE_CLOUDMSG_AMQP_FULL);

	skyze_app_source_add(app, source); // read video stream from rtsp device
	skyze_app_sink_add(app, sink); // output of video stream will be server via rtsp server
	skyze_app_inference_add(app, primary_gie); // run video frames through YOLOV3
	skyze_app_cloudmsg_add(app, cloudmsg); // publish realtime metadata to AMQP

	skyze_app_run(app); // runs pipeline, blocks forever

	return rc;
}

```


Milestone #4: Create an App from a yaml configuration file.
```
#include <libskyze_app.h>

int main(int argc, char *argv[])
{
	int rc = SKYZE_SUCCESS;
	skyze_app_t *app;

	app = skyze_app_new_from_config("/app/config.yaml");
	skyze_app_run(app); // runs pipeline, blocks forever

	return rc;
}

```
Milestone #Nn: Launch OpenSkyze via docker.  Supported in the Cloud and on NVIDIA Jetson devices.
```
docker run --rm --name openskyze -d -v $PWD/config.yaml:/app/config.yaml registry.gitlab.com/openskyze/openskyze/openskyze:latest

```
