# Proposal 1: Security System for Business/Residence use.

## Home/Business Security

## Description

One or more IP-based security cameras can be deployed to provide a cost effective video security system.  

## Primary Actor: Home/Business user
**Preconditions**: IP-based Video security system already installed and running on LAN.

**Postconditions**: User receives email alerts from OpenSkyze. 

**Main Scenarios:**
1. User Adds Each network connect camera to yaml configuration.
2. User configures the type of notications which will be emailed.
3. User launches OpenSkyze Software with Configuration.



```plantuml
actor User

rectangle OpenSkyze {
(5. Email Notification)
(4. Activity Detected)
(3. Start Service)
(2. Enable Email Notifications)
(1. Add Camera Config)

}
(5. Email Notification) -> User
(4. Activity Detected) -> (5. Email Notification)
User -> (3. Start Service)
User -> (2. Enable Email Notifications)
User -> (1. Add Camera Config)


```
