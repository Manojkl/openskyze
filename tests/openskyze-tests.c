#include <stdlib.h>
#include <stdio.h>

#include <glib.h>

#include <openskyze/openskyze.h>

// libopenskyze_config tests
static void config_load_null_uri() {
	g_assert(skyze_config_load(NULL)  == NULL);
}

static void config_load_valid_uri() {
	skyze_config_t *config = NULL;


	config = skyze_config_load("default.yaml");

	g_assert(config != NULL);

	skyze_config_free(config);
}

static void config_get_sources_single() {
	skyze_config_t *config = NULL;
	skyze_config_source_t *sources = NULL;
	int n_sources = 0;
	int rc = SKYZE_SUCCESS;

	config = skyze_config_load("single_source.yaml");

	g_assert(config != NULL);

	rc = skyze_config_get_sources(config, &sources, &n_sources);

	g_assert(rc == SKYZE_SUCCESS);

	skyze_config_free(config);
}

static void config_get_sinks_single() {
	skyze_config_t *config = NULL;
	skyze_config_sink_t *sinks = NULL;
	int n_sinks = 0;
	int rc = SKYZE_SUCCESS;

	config = skyze_config_load("single_sink.yaml");

	g_assert(config != NULL);

	rc = skyze_config_get_sinks(config, &sinks, &n_sinks);

	g_assert(rc == SKYZE_SUCCESS);

	skyze_config_free(config);
}
// libopenskyze_app tests
static void app_new() {
	skyze_app_t *app = NULL;

	app = skyze_app_new();

	g_assert(app != NULL);

	skyze_app_free(app);
}

static void app_new_with_config() {
	skyze_app_t *app = NULL;

	app = skyze_app_new_with_config("default.yaml");

	g_assert(app != NULL);

	skyze_app_free(app);
}

static void app_source_add() {
	skyze_app_t *app = NULL;
	skyze_source_t *source = NULL;
	int rc = SKYZE_SUCCESS;

	app = skyze_app_new();

	g_assert(app != NULL);

	source = skyze_source_new();
	
	g_assert(source != NULL);

	rc = skyze_app_source_add(app, source);
	
	g_assert(rc == SKYZE_SUCCESS);

	skyze_app_free(app);
	skyze_source_free(source);
}

static void app_source_add_invalid() {
	skyze_app_t *app = NULL;
	skyze_source_t *source = NULL;
	int rc = SKYZE_SUCCESS;

	app = skyze_app_new();

	g_assert(app != NULL);

	source = skyze_source_new();
	
	g_assert(source != NULL);

	rc = skyze_app_source_add(app, source);
	
	g_assert(rc != SKYZE_SUCCESS);

	skyze_app_free(app);
	skyze_source_free(source);
}

static void app_sink_add() {
	skyze_app_t *app = NULL;
	skyze_sink_t *sink = NULL;
	int rc = SKYZE_SUCCESS;

	app = skyze_app_new();

	g_assert(app != NULL);

	sink = skyze_sink_new();

	g_assert(sink != NULL);

	rc = skyze_app_sink_add(app, sink);

	g_assert(rc == SKYZE_SUCCESS);

	skyze_app_free(app);
	skyze_sink_free(sink);
}

// libopenskyze_source tests
static void source_new() {
	skyze_source_t *source = NULL;

	source = skyze_source_new();

	g_assert(source != NULL);

	skyze_source_free(source);
}

static void source_is_valid() {
	skyze_source_t *source = NULL;

	source = skyze_source_new();

	g_assert(source != NULL);

	skyze_source_free(source);
}

static void source_is_invalid() {
	int rc = SKYZE_SUCCESS;

	skyze_source_t *source = NULL;

	source = skyze_source_new();

	g_assert(source != NULL);

	rc = skyze_source_is_valid(source);

	g_assert(rc != SKYZE_SUCCESS);

	skyze_source_free(source);
}

// libopenskyze_source tests
static void sink_new() {
	skyze_sink_t *sink = NULL;

	sink = skyze_sink_new();

	g_assert(sink != NULL);

	skyze_sink_free(sink);
}

static void cleanup() {
	skyze_deinit();
}

int main(int argc, char *argv[]) {
	int rc = 0;

	g_test_init(&argc, &argv, NULL);

	skyze_init();

	g_test_add_func("/config/load_null_uri", config_load_null_uri);
	//g_test_add_func("/config/get_sources_single", config_get_sources_single);
	//g_test_add_func("/config/get_sinks_single", config_get_sinks_single);
	//g_test_add_func("/config/load_valid_uri", config_load_valid_uri);
	g_test_add_func("/app/new", app_new);
	//g_test_add_func("/app/new_with_config", app_new_with_config);
	//g_test_add_func("/app/source_add", app_source_add);
	g_test_add_func("/app/source_add_invalid", app_source_add_invalid);
	g_test_add_func("/app/sink_add", app_sink_add);
	g_test_add_func("/source/new", source_new);
	//g_test_add_func("/source/is_valid", source_is_valid);
	g_test_add_func("/source/is_invalid", source_is_invalid);
	g_test_add_func("/sink/new", sink_new);

	rc = g_test_run();

	cleanup();

	return rc;
}
