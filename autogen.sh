set -e

autoreconf --install --force --verbose -I m4
automake --add-missing
