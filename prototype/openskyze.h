#ifndef _CASCADE_H_
#define _CASCADE_H_

#include <gst/gst.h>
#include "deepstream_config.h"

typedef struct
{
  gint anomaly_count;
  gint meta_number;
  struct timespec timespec_first_frame;
  GstClockTime gst_ts_first_frame;
  GMutex lock_stream_rtcp_sr;
  guint32 id;
  gint frameCount;
  GstClockTime last_ntp_time;
} StreamSourceInfo;

typedef struct
{
  StreamSourceInfo streams[MAX_SOURCE_BINS];
} TestAppCtx;

struct timespec extract_utc_from_uri (gchar * uri);

#endif /**< _CASCADE_H_ */
