<h2 align="center">Supporting OpenSkyze</h2>

OpenSkyze is an MIT-licensed open source project with its ongoing development made possible entirely by volunteers.


## OpenSkyze

OpenSkyze is a platform that provides a cloud solution that implements the NVIDIA Deepstream platform.


Alpha Warning:  OpenSkyze is in the working prototype stage.


## Design

OpenSkyze is based off the [NVIDIA Deepstream SDK](https://developer.nvidia.com/deepstream-sdk).  The Deepstream SDK contains several reference applications, but are not in a state that we consider production ready.  The goal of the design is to develop libraries and APIs that can be easily understood and used.

The library will be developed using C and will maximize the use of GLib since Deepstream is implemented using GStreamer, which already makes use of GLib.  We also plan on providing bindings for popular languages such as python and golang.  We would also be happy to receive contributions that add support for additional bindings.

The OpenSkyze API will be broken down into the following components:

* Config - YAML-based configuration files using [libcyaml](https://github.com/tlsa/libcyaml).  The configuration objects will be broken down into the following objects:
  * application
  * osd
  * streammux
  * primary-gie
  * tracker
  * sinkX
  * sourceX
* VideoSource - Abstraction of Video sources. Currently this list includes:
   * CameraV4L2
   * URI
* VideoSink
  * FakeSink 
  * EglSink 
  * UDPSink 
  * nvoverlaysink 
  * MsgConvBroker
* Protocol Adapter - Implement protocol adapter using [NvMsgBroker API](https://docs.nvidia.com/metropolis/deepstream/dev-guide/text/DS_plugin_gst-nvmsgbroker.html) based on [librabbitmq-c](https://github.com/alanxz/rabbitmq-c).  Instead of JSON, nanopb (protobuf) will be used for message format.
  

## Stay In Touch

- [Twitter](https://twitter.com/nibbleshift)
- [Website](https://openskyze.com)


## License

[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2020-present, Steve McDaniel


## Building Notes
```
docker run --gpus all -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/src/openskyze:/opt/nvidia/deepstream/deepstream/sources/apps/sample_apps -e DISPLAY=$DISPLAY -w /opt/nvidia/deepstream/deepstream/sources/apps/sample_apps  nvcr.io/nvidia/deepstream:5.0.1-20.09-devel make
```

## Developing on Xavier
```
cd /opt/nvidia/deepstream/deepstream/sources/apps/sample_apps
git clone git@gitlab.com:openskyze/openskyze.git
cd openskyze
make 
./openskyze -c configs/default.txt
```

## Building yolo
```
cd /opt/nvidia/deepstream/deepstream-5.0/sources/objectDetector_Yolo/nvdsinfer_custom_impl_Yolo
CUDA_VER=10.2 make -j
```


## Starting RabbitMQ
OpenSkyze will publish all of the analytics via AMQP.  You'll need to launch the server before starting openskyze.  The management UI will be available at: http://localhost:15672/
```
docker run -d --network host --hostname rabbit --name rabbit -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_PASS=guest rabbitmq:3-management
```

