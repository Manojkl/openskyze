#include <glib.h>

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

#include <gst/rtsp-server/rtsp-server.h>

#include <openskyze/openskyze.h>

#define MAX_SINK_BINS (1024)


struct _skyze_sink_t {
    skyze_config_sink_t *config;
    GstElement *bin;
    GstElement *queue;
    GstElement *transform;
    GstElement *cap_filter;
    GstElement *enc_caps_filter;
    GstElement *encoder;
    GstElement *codecparse;
    GstElement *mux;
    GstElement *sink;
    GstElement *rtppay;

    GstElement *tee;
    gulong sink_buffer_probe;
};

skyze_sink_t *skyze_sink_new() {
    skyze_sink_t *sink = NULL;
    int rc = SKYZE_SUCCESS;

    sink = g_try_new0(skyze_sink_t, 1);

    if (sink == NULL) {
        rc = SKYZE_NO_MEM;
        goto exit;
    }

exit:
    if (rc != SKYZE_SUCCESS) {
        if (sink) {
            skyze_sink_free(sink);
            sink = NULL;
        }
    }
    return sink;
}

void skyze_sink_free(skyze_sink_t *sink)
{
    if (sink) {
        if (sink->config) {
            sink->config = NULL;
        }
        g_free(sink);
        sink = NULL;
    }
}

/*
   static guint uid = 0;
   static guint server_count = 0;
   static GMutex server_cnt_lock;
   static GstRTSPServer *server [MAX_SINK_BINS];
 */
/*
   static gboolean
   create_render_bin (skyze_config_sink_t * config, skyze_sink_t *sink)
   {
   gboolean ret = FALSE;
   gchar elem_name[50];
   GstElement *connect_to;
   GstCaps *caps = NULL;

   uid++;

   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin%d", uid);
   bin->bin = gst_bin_new (elem_name);
   if (!bin->bin) {
   g_print("Failed to create '%s'", elem_name);
   goto exit;
   }

   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_sink%d", uid);
   switch (config->type) {
   case SKYZE_SINK_RENDER_EGL:
   g_print("NVvideo renderer\n");
   bin->sink = gst_element_factory_make (NVDS_ELEM_SINK_EGL, elem_name);
   g_object_set (G_OBJECT (bin->sink), "window-x", config->offset_x,
   "window-y", config->offset_y, "window-width", config->width,
   "window-height", config->height, NULL);
   g_object_set (G_OBJECT (bin->sink), "enable-last-sample", FALSE, NULL);
   break;
   case SKYZE_SINK_RENDER_OVERLAY:
#ifndef PLATFORM_TEGRA
g_print("Overlay is only supported for Jetson");
return FALSE;
#endif
g_print(NVDS_APP, "NVvideo renderer\n");
bin->sink = gst_element_factory_make (NVDS_ELEM_SINK_OVERLAY, elem_name);
g_object_set (G_OBJECT (bin->sink), "display-id", config->display_id,
NULL);
g_object_set (G_OBJECT (bin->sink), "overlay", config->overlay_id, NULL);
g_object_set (G_OBJECT (bin->sink), "overlay-x", config->offset_x, NULL);
g_object_set (G_OBJECT (bin->sink), "overlay-y", config->offset_y, NULL);
g_object_set (G_OBJECT (bin->sink), "overlay-w", config->width, NULL);
g_object_set (G_OBJECT (bin->sink), "overlay-h", config->height, NULL);
break;
case SKYZE_SINK_FAKE:
bin->sink = gst_element_factory_make (NVDS_ELEM_SINK_FAKESINK, elem_name);
g_object_set (G_OBJECT (bin->sink), "enable-last-sample", FALSE, NULL);
break;
default:
return FALSE;
}

if (!bin->sink) {
g_print("Failed to create '%s'", elem_name);
goto exit;
}

g_object_set (G_OBJECT (bin->sink), "sync", config->sync, "max-lateness", -1,
"async", FALSE, "qos", config->qos, NULL);

#ifndef PLATFORM_TEGRA
g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_cap_filter%d", uid);
bin->cap_filter = gst_element_factory_make ("capsfilter", elem_name);
if (!bin->cap_filter) {
g_print("Failed to create '%s'", elem_name);
goto exit;
}
gst_bin_add (GST_BIN (bin->bin), bin->cap_filter);
#endif

g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_transform%d", uid);
if (config->type == SKYZE_SINK_RENDER_EGL) {
#ifdef PLATFORM_TEGRA
bin->transform =
gst_element_factory_make (NVDS_ELEM_EGLTRANSFORM, elem_name);
#else
bin->transform = gst_element_factory_make ("nvvideoconvert", elem_name);
#endif
if (!bin->transform) {
    g_print("Failed to create '%s'", elem_name);
    goto exit;
}
gst_bin_add (GST_BIN (bin->bin), bin->transform);

#ifndef PLATFORM_TEGRA
caps = gst_caps_new_empty_simple ("video/x-raw");

GstCapsFeatures *feature = NULL;
feature = gst_caps_features_new (MEMORY_FEATURES, NULL);
gst_caps_set_features (caps, 0, feature);
g_object_set (G_OBJECT (bin->cap_filter), "caps", caps, NULL);

g_object_set (G_OBJECT (bin->transform), "gpu-id", config->gpu_id, NULL);
g_object_set (G_OBJECT (bin->transform), "nvbuf-memory-type",
        config->nvbuf_memory_type, NULL);
#endif
}

g_snprintf (elem_name, sizeof (elem_name), "render_queue%d", uid);
bin->queue = gst_element_factory_make ("queue", elem_name);
if (!bin->queue) {
    g_print("Failed to create '%s'", elem_name);
    goto exit;
}

gst_bin_add_many (GST_BIN (bin->bin), bin->queue, bin->sink, NULL);

connect_to = bin->sink;

if (bin->cap_filter) {
    SKYZE_LINK_ELEMENT (bin->cap_filter, connect_to);
    connect_to = bin->cap_filter;
}

if (bin->transform) {
    SKYZE_LINK_ELEMENT (bin->transform, connect_to);
    connect_to = bin->transform;
}

SKYZE_LINK_ELEMENT (bin->queue, connect_to);

SKYZE_BIN_ADD_GHOST_PAD (bin->bin, bin->queue, "sink");

ret = TRUE;

exit:
if (caps) {
    gst_caps_unref (caps);
}
if (!ret) {
    g_print("%s failed", __func__);
}
return ret;
}

static void broker_queue_overrun (GstElement* sink_queue, gpointer user_data)
{
    (void)sink_queue;
    (void)user_data;
    g_print("nvmsgbroker queue overrun; Older Message Buffer "
            "Dropped; Network bandwidth might be insufficient\n");
}
*/

/*
   static gboolean
   create_msg_conv_broker_bin (NvDsSinkMsgConvBrokerConfig *config,
   skyze_sink_t *bin)
   {
   gboolean ret = FALSE;
   gchar elem_name[50];

   uid++;

   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin%d", uid);
   bin->bin = gst_bin_new (elem_name);
   if (!bin->bin) {
   g_print("Failed to create '%s'", elem_name);
   goto exit;
   }
   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_queue%d", uid);
   bin->queue = gst_element_factory_make ("queue", elem_name);
   if (!bin->queue) {
   g_print("Failed to create '%s'", elem_name);
   goto exit;
   }

   g_object_set(G_OBJECT(bin->queue), "leaky", 2, NULL);
   g_object_set(G_OBJECT(bin->queue), "max-size-buffers", 20, NULL);
   g_signal_connect (G_OBJECT (bin->queue), "overrun",
   G_CALLBACK (broker_queue_overrun), bin);

   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_transform%d", uid);
   if (config->disable_msgconv)
   {
   bin->transform = gst_element_factory_make ("queue", elem_name);
   }
   else
   {
   bin->transform = gst_element_factory_make (NVDS_ELEM_MSG_CONV, elem_name);
   }
   if (!bin->transform) {
   g_print("Failed to create '%s'", elem_name);
   goto exit;
   }

   if (!config->disable_msgconv)
   g_object_set (G_OBJECT(bin->transform), "config", config->config_file_path,
   "msg2p-lib", (config->conv_msg2p_lib ? config->conv_msg2p_lib : NULL),
   "payload-type", config->conv_payload_type,
   "comp-id", config->conv_comp_id,
   "debug-payload-dir", config->debug_payload_dir,
   "multiple-payloads", config->multiple_payloads,
   NULL);

   g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_sink%d", uid);
   bin->sink = gst_element_factory_make (NVDS_ELEM_MSG_BROKER, elem_name);
   if (!bin->sink) {
   g_print("Failed to create '%s'", elem_name);
   goto exit;
   }
   g_object_set (G_OBJECT(bin->sink), "proto-lib", config->proto_lib,
   "conn-str", config->conn_str,
   "topic", config->topic,
   "sync", config->sync, "async", FALSE,
   "config", config->broker_config_file_path,
   "comp-id", config->broker_comp_id,
   "new-api", config->new_api,
   NULL);

   gst_bin_add_many (GST_BIN (bin->bin),
   bin->queue, bin->transform, bin->sink,
   NULL);

   SKYZE_LINK_ELEMENT (bin->queue, bin->transform);
SKYZE_LINK_ELEMENT (bin->transform, bin->sink);

SKYZE_BIN_ADD_GHOST_PAD (bin->bin, bin->queue, "sink");

ret = TRUE;

exit:
if (!ret) {
    g_print("%s failed", __func__);
}
return ret;
}

    static gboolean
create_encode_file_bin (skyze_config_sink_t * config, skyze_sink_t *sink)
{
    GstCaps *caps = NULL;
    gboolean ret = FALSE;
    gchar elem_name[50];
    gulong bitrate = config->bitrate;
    guint profile = config->profile;

    uid++;

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin%d", uid);
    bin->bin = gst_bin_new (elem_name);
    if (!bin->bin) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_queue%d", uid);
    bin->queue = gst_element_factory_make ("queue", elem_name);
    if (!bin->queue) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_transform%d", uid);
    bin->transform = gst_element_factory_make ("nvvideoconvert", elem_name);
    if (!bin->transform) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_cap_filter%d", uid);
    bin->cap_filter = gst_element_factory_make ("capsfilter", elem_name);
    if (!bin->cap_filter) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }
    if (config->codec == SKYZE_ENCODER_MPEG4 || config->enc_type == SKYZE_ENCODER_TYPE_SW)
        caps = gst_caps_from_string ("video/x-raw, format=I420");
    else
        caps = gst_caps_from_string ("video/x-raw(memory:NVMM), format=I420");
    g_object_set (G_OBJECT (bin->cap_filter), "caps", caps, NULL);

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_encoder%d", uid);
    switch (config->codec) {
        case SKYZE_ENCODER_H264:
            if (config->enc_type == SKYZE_ENCODER_TYPE_SW)
                bin->encoder = gst_element_factory_make ("x264enc", elem_name);
            else
                bin->encoder = gst_element_factory_make ("nvv4l2h264enc", elem_name);
            break;
        case SKYZE_ENCODER_H265:
            if (config->enc_type == SKYZE_ENCODER_TYPE_SW)
                bin->encoder = gst_element_factory_make ("x265enc", elem_name);
            else
                bin->encoder = gst_element_factory_make ("nvv4l2h265enc", elem_name);
            break;
        case SKYZE_ENCODER_MPEG4:
            bin->encoder = gst_element_factory_make (NVDS_ELEM_ENC_MPEG4, elem_name);
            break;
        default:
            goto exit;
    }
    if (!bin->encoder) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

#ifdef PLATFORM_TEGRA
    g_object_set (G_OBJECT (bin->encoder), "bufapi-version", 1, NULL);
#endif

    if (config->codec == SKYZE_ENCODER_MPEG4)
        config->enc_type = SKYZE_ENCODER_TYPE_SW;

    if (config->enc_type == SKYZE_ENCODER_TYPE_HW) {
        g_object_set (G_OBJECT (bin->encoder), "profile", profile, NULL);
        g_object_set (G_OBJECT (bin->encoder), "iframeinterval", config->iframeinterval, NULL);
        g_object_set (G_OBJECT (bin->encoder), "bitrate", bitrate, NULL);
    } else {
        if (config->codec == SKYZE_ENCODER_MPEG4)
            g_object_set (G_OBJECT (bin->encoder), "bitrate", bitrate, NULL);
        else {
            //bitrate is in kbits/sec for software encoder "x264enc" and "x265enc"
            g_object_set (G_OBJECT (bin->encoder), "bitrate", bitrate / 1000, NULL);
        }
    }

    switch (config->codec) {
        case SKYZE_ENCODER_H264:
            bin->codecparse = gst_element_factory_make ("h264parse", "h264-parser");
            break;
        case SKYZE_ENCODER_H265:
            bin->codecparse = gst_element_factory_make ("h265parse", "h265-parser");
            break;
        case SKYZE_ENCODER_MPEG4:
            bin->codecparse = gst_element_factory_make ("mpeg4videoparse", "mpeg4-parser");
            break;
        default:
            goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_mux%d", uid);
    switch (config->container) {
        case SKYZE_CONTAINER_MP4:
            bin->mux = gst_element_factory_make (NVDS_ELEM_MUX_MP4, elem_name);
            break;
        case SKYZE_CONTAINER_MKV:
            bin->mux = gst_element_factory_make (NVDS_ELEM_MKV, elem_name);
            break;
        default:
            goto exit;
    }
    if (!bin->mux) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_sink%d", uid);
    bin->sink = gst_element_factory_make (NVDS_ELEM_SINK_FILE, elem_name);
    if (!bin->sink) {
        g_print("Failed to create '%s'", elem_name);
        goto exit;
    }

    g_object_set (G_OBJECT (bin->sink), "location", config->output_file_path,
            "sync", config->sync, "async", FALSE, NULL);
    g_object_set (G_OBJECT (bin->transform), "gpu-id", config->gpu_id, NULL);
    gst_bin_add_many (GST_BIN (bin->bin), bin->queue,
            bin->transform, bin->codecparse, bin->cap_filter,
            bin->encoder, bin->mux, bin->sink, NULL);

    SKYZE_LINK_ELEMENT (bin->queue, bin->transform);

    SKYZE_LINK_ELEMENT (bin->transform, bin->cap_filter);
    SKYZE_LINK_ELEMENT (bin->cap_filter, bin->encoder);

    SKYZE_LINK_ELEMENT (bin->encoder, bin->codecparse);
    SKYZE_LINK_ELEMENT (bin->codecparse, bin->mux);
    SKYZE_LINK_ELEMENT (bin->mux, bin->sink);

    SKYZE_BIN_ADD_GHOST_PAD (bin->bin, bin->queue, "sink");

    ret = TRUE;

exit:
    if (caps) {
        gst_caps_unref (caps);
    }
    if (!ret) {
        g_print("%s failed", __func__);
    }
    return ret;
}
*/

    static gboolean
start_rtsp_streaming (guint rtsp_port_num, guint updsink_port_num,
        SkyzeEncoderType enctype, guint64 udp_buffer_size)
{
    GstRTSPMountPoints *mounts;
    GstRTSPMediaFactory *factory;
    char udpsrc_pipeline[512];
    char port_num_Str[64] = { 0 };
    char *encoder_name;
    static GstRTSPServer *server;

    if (enctype == SKYZE_ENCODER_H264) {
        encoder_name = "H264";
    } else if (enctype == SKYZE_ENCODER_H265) {
        encoder_name = "H265";
    } else {
        g_print("%s failed\n", __func__);
        return FALSE;
    }

    if (udp_buffer_size == 0)
        udp_buffer_size = 512 * 1024;

    sprintf (udpsrc_pipeline,
            "( udpsrc name=pay0 port=%d buffer-size=%lu caps=\"application/x-rtp, media=video, "
            "clock-rate=90000, encoding-name=%s, payload=96 \" )",
            updsink_port_num, udp_buffer_size, encoder_name);

    sprintf (port_num_Str, "%d", rtsp_port_num);

    server = gst_rtsp_server_new ();
    g_object_set (server, "service", port_num_Str, NULL);

    mounts = gst_rtsp_server_get_mount_points (server);

    factory = gst_rtsp_media_factory_new ();
    gst_rtsp_media_factory_set_launch (factory, udpsrc_pipeline);

    gst_rtsp_mount_points_add_factory (mounts, "/openskyze", factory);

    g_object_unref (mounts);

    gst_rtsp_server_attach (server, NULL);

    g_print("\n *** DeepStream: Launched RTSP Streaming at rtsp://localhost:%d/openskyze ***\n\n",
            rtsp_port_num);

    return TRUE;
}


skyze_sink_t *skyze_sink_rtsp_new()
{
    skyze_sink_t *sink = NULL;

    sink = skyze_sink_new();

    return sink;
}

skyze_sink_t *skyze_sink_udp_new(skyze_config_sink_t * config)
{
    GstCaps *caps = NULL;
    gboolean ret = FALSE;
    gchar elem_name[50];
    gchar encode_name[50];
    gchar rtppay_name[50];
    gint uid = 0;
    skyze_sink_t *sink = NULL;
    int rc = SKYZE_SUCCESS;

    sink = skyze_sink_new();

    if (sink == NULL) {
        rc = SKYZE_INVALID_ARGS;
        goto exit;
    }

    //guint rtsp_port_num = g_rtsp_port_num++;
    uid++;

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin%d", uid);
    sink->bin = gst_bin_new (elem_name);
    if (!sink->bin) {
        g_print("Failed to create '%s'\n", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_queue%d", uid);
    sink->queue = gst_element_factory_make ("queue", elem_name);
    if (!sink->queue) {
        g_print("Failed to create '%s'\n", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_transform%d", uid);
    sink->transform = gst_element_factory_make ("nvvideoconvert", elem_name);
    if (!sink->transform) {
        g_print("Failed to create '%s'\n", elem_name);
        goto exit;
    }

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_cap_filter%d", uid);
    sink->cap_filter = gst_element_factory_make ("capsfilter", elem_name);
    if (!sink->cap_filter) {
        g_print("Failed to create '%s'\n", elem_name);
        goto exit;
    }

    if (config->enc_type == SKYZE_ENCODER_TYPE_SW)
        caps = gst_caps_from_string ("video/x-raw, format=I420");
    else
        caps = gst_caps_from_string ("video/x-raw(memory:NVMM), format=I420");

    g_object_set (G_OBJECT (sink->cap_filter), "caps", caps, NULL);

    g_snprintf (encode_name, sizeof (encode_name), "sink_sub_bin_encoder%d", uid);
    g_snprintf (rtppay_name, sizeof (rtppay_name), "sink_sub_bin_rtppay%d", uid);

    switch (config->codec) {
        case SKYZE_ENCODER_H264:
            sink->codecparse = gst_element_factory_make ("h264parse", "h264-parser");
            sink->rtppay = gst_element_factory_make ("rtph264pay", rtppay_name);
            if (config->enc_type == SKYZE_ENCODER_TYPE_SW)
                sink->encoder = gst_element_factory_make ("x264enc", encode_name);
            else
                sink->encoder = gst_element_factory_make ("nvv4l2h264enc", encode_name);
            break;
        case SKYZE_ENCODER_H265:
            sink->codecparse = gst_element_factory_make ("h265parse", "h265-parser");
            sink->rtppay = gst_element_factory_make ("rtph265pay", rtppay_name);
            if (config->enc_type == SKYZE_ENCODER_TYPE_SW)
                sink->encoder = gst_element_factory_make ("x265enc", encode_name);
            else
                sink->encoder = gst_element_factory_make ("nvv4l2h265enc", encode_name);
            break;
        default:
            goto exit;
    }

    if (!sink->encoder) {
        g_print("Failed to create '%s'\n", encode_name);
        goto exit;
    }

    if (!sink->rtppay) {
        g_print("Failed to create '%s'\n", rtppay_name);
        goto exit;
    }

    if (config->enc_type  == SKYZE_ENCODER_TYPE_SW) {
        //bitrate is in kbits/sec for software encoder "x264enc" and "x265enc"
        g_object_set (G_OBJECT (sink->encoder), "bitrate", config->bitrate/1000, NULL);
    } else {
        g_object_set (G_OBJECT (sink->encoder), "bitrate", config->bitrate, NULL);
        g_object_set (G_OBJECT (sink->encoder), "profile", config->profile, NULL);
        g_object_set (G_OBJECT (sink->encoder), "iframeinterval", config->iframeinterval, NULL);
    }

#ifdef PLATFORM_TEGRA
    g_object_set (G_OBJECT (sink->encoder), "preset-level", 1, NULL);
    g_object_set (G_OBJECT (sink->encoder), "insert-sps-pps", 1, NULL);
    g_object_set (G_OBJECT (sink->encoder), "bufapi-version", 1, NULL);
#else
    g_object_set (G_OBJECT (sink->transform), "gpu-id", config->gpu_id, NULL);
#endif

    g_snprintf (elem_name, sizeof (elem_name), "sink_sub_bin_udpsink%d", uid);
    sink->sink = gst_element_factory_make ("udpsink", elem_name);
    if (!sink->sink) {
        g_print("Failed to create '%s'\n", elem_name);
        goto exit;
    }

    g_object_set (G_OBJECT (sink->sink), "host", "224.224.255.255", "port",
            config->udp_port, "async", FALSE, "sync", 0, NULL);

    gst_bin_add_many (GST_BIN (sink->bin),
            sink->queue, sink->cap_filter, sink->transform,
            sink->encoder, sink->codecparse, sink->rtppay, sink->sink, NULL);

    SKYZE_LINK_ELEMENT (sink->queue, sink->transform);
    SKYZE_LINK_ELEMENT (sink->transform, sink->cap_filter);
    SKYZE_LINK_ELEMENT (sink->cap_filter, sink->encoder);
    SKYZE_LINK_ELEMENT (sink->encoder, sink->codecparse);
    SKYZE_LINK_ELEMENT (sink->codecparse, sink->rtppay);
    SKYZE_LINK_ELEMENT (sink->rtppay, sink->sink);

    SKYZE_BIN_ADD_GHOST_PAD (sink->bin, sink->queue, "sink");

    ret = TRUE;

    ret = start_rtsp_streaming (config->rtsp_port, config->udp_port, config->codec,
            config->udp_buffer_size);
    if (ret != TRUE) {
        g_print ("%s: start_rtsp_straming function failed\n", __func__);
    }

exit:
    if (caps) {
        gst_caps_unref (caps);
    }
    if (!ret) {
        g_print("%s failed\n", __func__);
    }
    return sink;
}
/*
gboolean create_sink_bin (guint num_sub_bins, skyze_config_sink_t * config,
        skyze_sink_t *sink, guint index)
{
    gboolean ret = FALSE;
    guint i;

    sink->bin = gst_bin_new ("sink_bin");
    if (!sink->bin) {
        g_print("Failed to create element 'sink_bin'");
        goto exit;
    }

    sink->queue = gst_element_factory_make ("queue", "sink_bin_queue");
    if (!sink->queue) {
        g_print("Failed to create element 'sink_bin_queue'");
        goto exit;
    }

    gst_bin_add (GST_BIN (sink->bin), sink->queue);

    SKYZE_BIN_ADD_GHOST_PAD (sink->bin, sink->queue, "sink");

    sink->tee = gst_element_factory_make ("tee", "sink_bin_tee");
    if (!sink->tee) {
        g_print("Failed to create element 'sink_bin_tee'");
        goto exit;
    }

    gst_bin_add (GST_BIN (sink->bin), sink->tee);

    SKYZE_LINK_ELEMENT (sink->queue, sink->tee);

    g_object_set (G_OBJECT (sink->tee), "allow-not-linked", TRUE, NULL);

    if (config->enable && !config->link_to_demux) {
        switch (config->type) {
            case SKYZE_SINK_RENDER_EGL:
            case SKYZE_SINK_RENDER_OVERLAY:
            case SKYZE_SINK_FAKE:
                config->render_config.type = config->type;
                config->render_config.sync = config->sync;
                if (!create_render_bin (&config->render_config,
                            sink))
                    goto exit;
                break;
            
            case SKYZE_SINK_ENCODE_FILE:
                if (!create_encode_file_bin (config,
                            sink))
                    goto exit;
                break;
            case SKYZE_SINK_UDPSINK:
                if (!create_udpsink_bin (config,
                            sink))
                    goto exit;
                break;
            case SKYZE_SINK_MSG_CONV_BROKER:
                if (!create_msg_conv_broker_bin (config, sink))
                    goto exit;
                break;
            default:
                goto exit;
        }
    }

    if(config->type != SKYZE_SINK_MSG_CONV_BROKER) {
        gst_bin_add (GST_BIN (sink->bin), sink->bin);
        if (!link_element_to_tee_src_pad (sink->tee, sink->bin)) {
            goto exit;
        }
    }

    config.type = SKYZE_SINK_FAKE;
    if (!create_render_bin (&config, sink->bin))
        goto exit;
    gst_bin_add (GST_BIN (sink->bin), sink->bin);
    if (!link_element_to_tee_src_pad (sink->tee, sink->bin)) {
        goto exit;
    }

    ret = TRUE;
exit:
    if (!ret) {
        g_print("%s failed", __func__);
    }
    return ret;
}*/


/*
gboolean create_demux_sink_bin (guint num_sub_bins, skyze_config_sink_t * config,
        skyze_sink_t *sink, guint index)
{
    gboolean ret = FALSE;
    guint i;

    sink->bin = gst_bin_new ("sink_bin");
    if (!sink->bin) {
        g_print("Failed to create element 'sink_bin'");
        goto exit;
    }

    sink->queue = gst_element_factory_make ("queue", "sink_bin_queue");
    if (!sink->queue) {
        g_print("Failed to create element 'sink_bin_queue'");
        goto exit;
    }

    gst_bin_add (GST_BIN (sink->bin), sink->queue);

    SKYZE_BIN_ADD_GHOST_PAD (sink->bin, sink->queue, "sink");

    sink->tee = gst_element_factory_make ("tee", "sink_bin_tee");
    if (!sink->tee) {
        g_print("Failed to create element 'sink_bin_tee'");
        goto exit;
    }

    gst_bin_add (GST_BIN (sink->bin), sink->tee);

    SKYZE_LINK_ELEMENT (sink->queue, sink->tee);

    if (config->link_to_demux) {
        switch (config->type) {
            case SKYZE_SINK_RENDER_EGL:
            case SKYZE_SINK_RENDER_OVERLAY:
            case SKYZE_SINK_FAKE:
                if (!create_render_bin (config,
                            sink))
                    goto exit;
                break;
            case SKYZE_SINK_ENCODE_FILE:
                if (!create_encode_file_bin (config,
                            sink))
                    goto exit;
                break;
            case SKYZE_SINK_UDPSINK:
                if (!create_udpsink_bin (config, sink))
                    goto exit;
                break;
            case SKYZE_SINK_MSG_CONV_BROKER:
                if (!create_msg_conv_broker_bin (config,
                            sink))
                    goto exit;
                break;
            default:
                goto exit;
        }

        if(config->type != SKYZE_SINK_MSG_CONV_BROKER) {
            gst_bin_add (GST_BIN (sink->bin), sink->bin);
            if (!link_element_to_tee_src_pad (sink->tee, sink->bin)) {
                goto exit;
            }
        }
    }

       if (sink->num_bins == 0) {
       config->type = SKYZE_SINK_FAKE;
       if (!create_render_bin (&config, sink->bin))
       goto exit;
       gst_bin_add (GST_BIN (sink->bin), sink->bin);
       if (!link_element_to_tee_src_pad (sink->tee, sink->bin)) {
       goto exit;
       }
       sink->num_bins = 1;
       }

    ret = TRUE;
exit:
    if (!ret) {
        g_print("%s failed", __func__);
    }
    return ret;
}*/

static GstRTSPFilterResult client_filter (GstRTSPServer * server, GstRTSPClient * client, gpointer user_data)
{
    return GST_RTSP_FILTER_REMOVE;
}

/*
   void
   destroy_sink_bin ()
   {
   GstRTSPMountPoints *mounts;
   GstRTSPSessionPool *pool;
   guint i = 0;
   for (i = 0; i < server_count; i++) {
   mounts = gst_rtsp_server_get_mount_points (server [i]);
   gst_rtsp_mount_points_remove_factory (mounts, "/openskyze");
   g_object_unref (mounts);
   gst_rtsp_server_client_filter (server [i], client_filter, NULL);
   pool = gst_rtsp_server_get_session_pool (server [i]);
   gst_rtsp_session_pool_cleanup (pool);
   g_object_unref (pool);
   }
   }*/
