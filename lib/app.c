#include <stdio.h>
#include <stdlib.h>

#include <glib.h>
#include <gst/gst.h>

#include <openskyze/openskyze.h>

typedef struct {
  guint index;
  gulong all_bbox_buffer_probe_id;
  gulong primary_bbox_buffer_probe_id;
  gulong fps_buffer_probe_id;
  GstElement *bin;
  GstElement *tee;
  GstElement *msg_conv;

  //NvDsPrimaryGieBin primary_gie_bin;
  //NvDsOSDBin osd_bin;
  //NvDsSecondaryGieBin secondary_gie_bin;
  //NvDsTrackerBin tracker_bin;
  //NvDsSinkBin sink_bin;
  //NvDsSinkBin demux_sink_bin;
} skyze_app_common_t;

struct _skyze_app_t {
    GstElement *pipeline;
    GstElement *streammux;
    GMainLoop *loop;
    char *padding;
    GList *sources;
    GList *sinks;
    skyze_config_t *config;
    GMutex lock;
    GCond cond;
    gboolean quit;
    int return_value;
    gint bus_id;
    GstBus *bus;

    /* sink */
    GstElement *sink_bin;
    GstElement *sink_queue;
    GstElement *sink_tee;

    /* sink */

    /* callback */
    gint primary_bbox_buffer_probe_id;
};

static gboolean bus_callback(GstBus *bus, GstMessage *message, gpointer data);
static gboolean set_streammux_properties (skyze_config_t *config, GstElement *element);


static int initialize_common(skyze_app_t *app) {
    int rc = SKYZE_SUCCESS;

    if (app == NULL) {
        rc = SKYZE_INVALID_ARGS;
        goto exit;
    }
exit:
	return rc;
}

static int initialize_sink(skyze_app_t *app) {
    int rc = SKYZE_SUCCESS;

    if (app == NULL) {
        rc = SKYZE_INVALID_ARGS;
        goto exit;
    }
    app->sink_bin = gst_bin_new ("sink_bin");
    if (!app->sink_bin) {
        rc = SKYZE_PIPELINE_FAILED;
        g_print("Failed to create element 'sink_bin'");
        goto exit;
    }

    app->sink_queue = gst_element_factory_make ("queue", "sink_bin_queue");
    if (!app->sink_queue) {
        rc = SKYZE_PIPELINE_FAILED;
        g_print("Failed to create element 'sink_bin_queue'");
        goto exit;
    }

    gst_bin_add (GST_BIN (app->sink_bin), app->sink_queue);

    SKYZE_BIN_ADD_GHOST_PAD (app->sink_bin, app->sink_queue, "sink");

    app->sink_tee = gst_element_factory_make ("tee", "sink_bin_tee");
    if (!app->sink_tee) {
        rc = SKYZE_PIPELINE_FAILED;
        g_print("Failed to create element 'sink_bin_tee'");
        goto exit;
    }

	gst_bin_add (GST_BIN (app->sink_bin), app->sink_tee);
	SKYZE_LINK_ELEMENT (app->sink_queue, app->sink_tee);

	g_object_set (G_OBJECT (app->sink_tee), "allow-not-linked", TRUE, NULL);

exit:
	return rc;
}

static int initialize_inference(skyze_app_t *app) {
	int rc = SKYZE_SUCCESS;

	if (app == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

exit:
	return rc;
}

skyze_app_t *skyze_app_new()
{
	skyze_app_t *app = NULL;
	int rc = SKYZE_SUCCESS;

	app = g_try_new0(skyze_app_t, 1);

	if (app == NULL) {
		rc = SKYZE_NO_MEM;
		goto exit;
	}

	app->pipeline = gst_pipeline_new("openskyze-pipeline");

	if (app->pipeline == NULL) {
		g_printerr("Failed to create pipeline\n");
		rc = SKYZE_PIPELINE_FAILED;
		goto exit;
	}

	app->bus = gst_pipeline_get_bus (GST_PIPELINE (app->pipeline));
	app->bus_id = gst_bus_add_watch (app->bus, bus_callback, app);
	gst_object_unref (app->bus);

	app->streammux = gst_element_factory_make ("nvstreammux", "stream-muxer");

	if (!app->streammux) {
		g_printerr("Failed to create muxer\n");
		rc = SKYZE_PIPELINE_FAILED;
		goto exit;
	}

	gst_bin_add (GST_BIN(app->pipeline), app->streammux);

	rc = initialize_sink(app);

	if (rc != SKYZE_SUCCESS) {
		g_printerr("Failed initializing sink\n");
		goto exit;
	}
	app->loop = g_main_loop_new(NULL, FALSE);

exit:
	if (rc != SKYZE_SUCCESS) {
		if (app) {
			skyze_app_free(app);
			app = NULL;
		}
	}
	return app;
}

skyze_app_t *skyze_app_new_with_config(const char *cfg_file)
{
	skyze_app_t *app = NULL;
	int rc = SKYZE_SUCCESS;
	skyze_config_source_t *sources = NULL;
	int n_sources = 0;
	skyze_config_sink_t *sinks = NULL;
	int n_sinks = 0;
	skyze_config_inference_t *inference = NULL;

	if (cfg_file == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	app = skyze_app_new();

	if (app == NULL) {
		rc = SKYZE_APP_INIT_FAILED;
		goto exit;
	}

	app->config = skyze_config_load(cfg_file);

	if (app->config == NULL) {
		rc = SKYZE_CONFIG_FAILED;
		goto exit;
	}

	rc = skyze_config_get_sources(app->config, &sources, &n_sources);

	if (rc != SKYZE_SUCCESS) {
		g_print("skyze_config_get_sources failed with rc=%d\n", rc);
		goto exit;
	}

	if (sources && n_sources < 1) {
		g_print("Error: Invalid config, requires at least one source.\n");
		rc = SKYZE_INVALID_CONFIG;
		goto exit;
	}

	for (int i = 0; i < n_sources; ++i) {
		skyze_source_t *source = NULL;
		skyze_config_source_t *src_config = &sources[i];

		source = skyze_source_new_with_config(src_config);

		if (source == NULL) {
			rc = SKYZE_INVALID_CONFIG;
			goto exit;
		}

		g_print("Added source '%s'\n", src_config->name);
		rc = skyze_app_source_add(app, source);

		if (rc != SKYZE_SUCCESS) {
			g_print("Failed adding source with %d\n", rc);
			goto exit;
		}
	}

	rc = skyze_config_get_sinks(app->config, &sinks, &n_sinks);

	if (rc != SKYZE_SUCCESS) {
		g_print("skyze_config_get_sinks failed with rc=%d\n", rc);
		goto exit;
	}

	if (sinks && n_sinks < 1) {
		g_print("Error: Invalid config, requires at least one sink.\n");
		rc = SKYZE_INVALID_CONFIG;
		goto exit;
	}

	for (int i = 0; i < n_sinks; ++i) {
		skyze_sink_t *sink = NULL;
		skyze_config_sink_t *src_config = &sinks[i];

		g_print("Added sink '%s'\n", src_config->name);
		rc = skyze_app_sink_add(app, sink);

		if (rc != SKYZE_SUCCESS) {
			g_print("Failed adding sink with %d\n", rc);
			goto exit;
		}
	}

	rc = skyze_config_get_inference(app->config, &inference);

	if (rc != SKYZE_SUCCESS) {
		g_print("skyze_config_get_sinks failed with rc=%d\n", rc);
		goto exit;
	}

exit:
	if (rc != SKYZE_SUCCESS) {
		if (app) {
			skyze_app_free(app);
			app = NULL;
		}
	}
	return app;
}

int skyze_app_run(skyze_app_t *app)
{
	int rc = SKYZE_SUCCESS;

	if (app == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	set_streammux_properties(app->config, app->streammux);

	if (app->return_value != -1) {
		if (gst_element_set_state (app->pipeline,
					GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
			g_print ("\ncan't set pipeline to playing state.\n");
			app->return_value = -1;
			goto exit;
		}
	}

	g_main_loop_run (app->loop);

exit:
	return rc;
}

int skyze_app_source_add(skyze_app_t *app, skyze_source_t *source)
{
	int rc = SKYZE_SUCCESS;
	static int i = 0;
	GstPad *sinkpad = NULL;
	GstPad *srcpad = NULL;
	gchar pad_name[16] = {0};

	if (app == NULL || source == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

	rc = skyze_source_is_valid(source);

	if (rc != SKYZE_SUCCESS) {
		goto exit;
	}

	if (!gst_bin_add (GST_BIN(app->pipeline), source->bin)) {
		rc = SKYZE_BIN_ADD_FAILED;
		goto exit;
	}

	g_snprintf (pad_name, 15, "sink_%u", i);

	sinkpad = gst_element_get_request_pad(app->streammux, pad_name);

	if (!sinkpad) {
		rc = SKYZE_REQUEST_PAD_FAILED;
		g_printerr ("Streammux request sink pad failed. Exiting.\n");
		goto exit;
	}

	srcpad = gst_element_get_static_pad (source->bin, "src");
	if (!srcpad) {
		rc = SKYZE_STATIC_PAD_FAILED;
		g_printerr ("Failed to get src pad of source bin. Exiting.\n");
		goto exit;
	}

	if (gst_pad_link (srcpad, sinkpad) != GST_PAD_LINK_OK) {
		rc = SKYZE_LINK_PAD_FAILED;
		g_printerr ("Failed to link source bin to stream muxer. Exiting.\n");
		goto exit;
	}

	gst_object_unref (srcpad);
	gst_object_unref (sinkpad);
exit:

	if (rc != SKYZE_SUCCESS) {
		if (srcpad) {
			gst_object_unref (srcpad);
		}

		if (sinkpad) {
			gst_object_unref (sinkpad);
		}
	}
	return rc;
}

int skyze_app_sink_add(skyze_app_t *app, skyze_sink_t *sink)
{
	int rc = SKYZE_SUCCESS;

	if (app == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

exit:
	return rc;
}

int skyze_app_inference_add(skyze_app_t *app, skyze_inference_t *inference)
{
	int rc = SKYZE_SUCCESS;
	GstElement **sink_elem = NULL;
	GstElement **src_elem = NULL;
    GstElement *inference_bin = NULL;

	if (app == NULL || inference == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}
	
    inference_bin = skyze_inference_get_bin(inference);

    if (inference_bin == NULL) {
        rc = SKYZE_BIN_ADD_FAILED;
        goto exit;
    }

	gst_bin_add (GST_BIN (app->pipeline), inference_bin);

	if (*sink_elem) {
		SKYZE_LINK_ELEMENT (inference_bin, *sink_elem);
	}

	*sink_elem = inference_bin;
	
	if (!*src_elem) {
		*src_elem = inference_bin;
	}

	SKYZE_ELEM_ADD_PROBE (app->primary_bbox_buffer_probe_id,
			inference_bin, "src",
			skyze_inference_get_probe(inference), GST_PAD_PROBE_TYPE_BUFFER,
			app);
exit:
	return rc;
}


int skyze_app_message_consumer_add(skyze_app_t *app)
{
	int rc = SKYZE_SUCCESS;

	if (app == NULL) {
		rc = SKYZE_INVALID_ARGS;
		goto exit;
	}

exit:
	return rc;
}

void skyze_app_free(skyze_app_t *app) {
	if (app) {
		if (app->loop) {
			g_main_loop_unref(app->loop);
		}

		if (app->pipeline) {
			gst_object_unref(GST_OBJECT(app->pipeline));
			app->pipeline = NULL;
		}

		if (app->config) {
			skyze_config_free(app->config);
			app->config = NULL;
		}
		g_free(app);
		app = NULL;
	}
}

static gboolean bus_callback (GstBus * bus, GstMessage * message, gpointer data)
{
	skyze_app_t *app = (skyze_app_t *) data;
	GstState oldstate, newstate;
	GError *error = NULL;
	gchar *debuginfo = NULL;
	guint i = 0;

	g_print("Received message on bus: source %s, msg_type %s\n", GST_MESSAGE_SRC_NAME (message), GST_MESSAGE_TYPE_NAME (message));

	switch (GST_MESSAGE_TYPE (message)) {
		case GST_MESSAGE_INFO:
			gst_message_parse_info (message, &error, &debuginfo);
			g_printerr ("INFO from %s: %s\n",
					GST_OBJECT_NAME (message->src), error->message);
			if (debuginfo) {
				g_printerr ("Debug info: %s\n", debuginfo);
			}
			g_error_free (error);
			g_free (debuginfo);
			break;
		case GST_MESSAGE_WARNING:
			gst_message_parse_warning (message, &error, &debuginfo);
			g_printerr ("WARNING from %s: %s\n",
					GST_OBJECT_NAME (message->src), error->message);
			if (debuginfo) {
				g_printerr ("Debug info: %s\n", debuginfo);
			}
			g_error_free (error);
			g_free (debuginfo);
			break;
		case GST_MESSAGE_ERROR:
			gst_message_parse_error (message, &error, &debuginfo);
			g_printerr ("ERROR from %s: %s\n",
					GST_OBJECT_NAME (message->src), error->message);
			if (debuginfo) {
				g_printerr ("Debug info: %s\n", debuginfo);
			}

			/*
			   NvDsSrcParentBin *bin = &app->pipeline.multi_src_bin;
			   GstElement *msg_src_elem = (GstElement *) GST_MESSAGE_SRC (message);
			   gboolean bin_found = FALSE;
			   while (msg_src_elem && !bin_found) {
			   for (i = 0; i < bin->num_bins && !bin_found; i++) {
			   if (bin->sub_bins[i].src_elem == msg_src_elem ||
			   bin->sub_bins[i].bin == msg_src_elem) {
			   bin_found = TRUE;
			   break;
			   }
			   }
			   msg_src_elem = GST_ELEMENT_PARENT (msg_src_elem);
			   }

			   if ((i != bin->num_bins) &&
			   (app->config.multi_source_config[0].type == SKYZE_SOURCE_RTSP)) {
			   NvDsSrcBin *subBin = &bin->sub_bins[i];

			   if (!subBin->reconfiguring ||
			   g_strrstr(debuginfo, "500 (Internal Server Error)")) {
			   subBin->reconfiguring = TRUE;
			   g_timeout_add (0, reset_source_pipeline, subBin);
			   }
			   g_error_free (error);
			   g_free (debuginfo);
			   return TRUE;
			   }

			   if (app->config.multi_source_config[0].type == SKYZE_SOURCE_CAMERA_V4L2) {
			   if (g_strrstr(debuginfo, "reason not-negotiated (-4)")) {
			   g_print ("incorrect camera parameters provided, please provide supported resolution and frame rate\n");
			   }

			   if (g_strrstr(debuginfo, "Buffer pool activation failed")) {
			   g_print ("usb bandwidth might be saturated\n");
			   }
			   }*/

			g_error_free (error);
			g_free (debuginfo);
			app->return_value = -1;
			app->quit = TRUE;
			break;
		case GST_MESSAGE_STATE_CHANGED:
			gst_message_parse_state_changed (message, &oldstate, &newstate, NULL);
			if (GST_ELEMENT (GST_MESSAGE_SRC (message)) == app->pipeline) {
				switch (newstate) {
					case GST_STATE_PLAYING:
						g_print ("Pipeline running\n");
						GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (app->pipeline), GST_DEBUG_GRAPH_SHOW_ALL,
								"openskyze-playing");
						break;
					case GST_STATE_PAUSED:
						if (oldstate == GST_STATE_PLAYING) {
							g_print ("Pipeline paused\n");
						}
						break;
					case GST_STATE_READY:
						GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (app->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "openskyze-ready");
						if (oldstate == GST_STATE_NULL) {
							g_print ("Pipeline ready\n");
						} else {
							g_print ("Pipeline stopped\n");
						}
						break;
					case GST_STATE_NULL:
						g_mutex_lock (&app->lock);
						g_cond_broadcast (&app->cond);
						g_mutex_unlock (&app->lock);
						break;
					default:
						break;
				}
			}
			break;
		case GST_MESSAGE_EOS:
			g_print ("Received EOS. Exiting ...\n");
			app->quit = TRUE;
			return FALSE;
			break;
		default:
			break;
	}
	return TRUE;
}

static gboolean set_streammux_properties (skyze_config_t *config, GstElement *element)
{
	gboolean ret = FALSE;

	g_object_set(G_OBJECT(element), "gpu-id",
			config->settings->streammux->gpu_id, NULL);

	g_object_set (G_OBJECT (element), "nvbuf-memory-type",
			config->settings->streammux->nvbuf_memory_type, NULL);

	g_object_set(G_OBJECT(element), "live-source",
			config->settings->streammux->live_source, NULL);

	g_object_set(G_OBJECT(element),
			"batched-push-timeout", config->settings->streammux->batched_push_timeout, NULL);

	if (config->settings->streammux->batch_size){
		g_object_set(G_OBJECT(element), "batch-size",
				config->settings->streammux->batch_size, NULL);
	}

	/*
	   g_object_set(G_OBJECT(element), "enable-padding",
	   config->settings->enable_padding, NULL);
	 */
	if (config->settings->streammux->width && config->settings->streammux->height) {
		g_object_set(G_OBJECT(element), "width",
				config->settings->streammux->width, NULL);
		g_object_set(G_OBJECT(element), "height",
				config->settings->streammux->height, NULL);
	}

	/*
	   g_object_set(G_OBJECT(element), "attach-sys-ts",
	   config->attach_sys_ts_as_ntp, NULL);
	 */

	return ret;
}

