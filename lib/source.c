#include <glib.h>

#include <openskyze/openskyze.h>

static void skyze_record_free(skyze_record_t *record);

skyze_source_t *skyze_source_new() {
	skyze_source_t *source = NULL;
	int rc = SKYZE_SUCCESS;

	source = g_try_new0(skyze_source_t, 1);

	if (source == NULL) {
		rc = SKYZE_NO_MEM;
		goto exit;
	}


exit:
	if (rc != SKYZE_SUCCESS) {
		if (source) {
			skyze_source_free(source);
			source = NULL;
		}
	}
	return source;
}

skyze_source_t *skyze_source_uri_new(skyze_config_source_t *config)
{
	skyze_source_t *source = NULL;
	int rc = SKYZE_SUCCESS;

	if (config == NULL) {
		rc = -1;
		goto exit;
	}

	source = skyze_source_new();

	if (source == NULL) {
		rc = SKYZE_NO_MEM;
		goto exit;
	}

	if (!create_rtsp_source(source, config)) {
	}


exit:
	if (rc != SKYZE_SUCCESS) {
		skyze_source_free(source);
		source = NULL;
	}
	return source;
}

static void skyze_record_free(skyze_record_t *record)
{
	if (record) {
		g_free(record);
		record = NULL;
	}
}

skyze_source_t *skyze_source_new_with_config(skyze_config_source_t *config)
{
	skyze_source_t *source = NULL;
	skyze_record_t *record = NULL;
	int rc = SKYZE_SUCCESS;

	if (config == NULL) {
		rc = -1;
		goto exit;
	}

	source = skyze_source_new();

	if (source == NULL) {
		rc = SKYZE_NO_MEM;
		goto exit;
	}


	source = skyze_source_new();
	source->config = config;
	//create_source_bin(config, source);
    create_rtsp_source(source, config);

exit:
	if (rc != SKYZE_SUCCESS) {
		skyze_source_free(source);
		source = NULL;
	}
	return source;
}

int skyze_source_is_valid(skyze_source_t *source)
{
	int rc = SKYZE_SUCCESS;

	if (source == NULL || source->config == NULL || source->bin == NULL) {
		rc = SKYZE_INVALID_SOURCE;
	}

	return rc;
}

void skyze_source_free(skyze_source_t *source)
{
	if (source) {
		if (source->config) {
			//skyze_config_source_free(source->config);
			source->config = NULL;
		}

		if (source->bin) {
			g_free(source->bin);
			source->bin = NULL;
		}

		g_free(source);
		source = NULL;
	}
}


