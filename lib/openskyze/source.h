#ifndef _SKYZE_SOURCE_H
#define _SKYZE_SOURCE_H

#include <glib.h>

#include <openskyze/config.h>

typedef struct _skyze_source_t skyze_source_t;
typedef struct _skyze_record_t skyze_record_t;

typedef enum
{
  SKYZE_SOURCE_CAMERA_V4L2 = 1,
  SKYZE_SOURCE_URI,
  SKYZE_SOURCE_URI_MULTIPLE,
  SKYZE_SOURCE_RTSP,
  SKYZE_SOURCE_CAMERA_CSI,
} SkyzeSourceType;

typedef enum 
{
	SKYZE_RECORD_MODE_CLOUD = 0,
	SKYZE_RECORD_MODE_LOCAL = 1,
	SKYZE_RECORD_MODE_CLOUD_LOCAL = 2,
} SkyzeRecordMode;

typedef enum
{
	SKYZE_RECORD_CONTAINER_MKV = 0,
	SKYZE_RECORD_CONTAINER_M4A = 1,
} SkyzeRecordContainer;

skyze_source_t *skyze_source_new();
skyze_source_t *skyze_source_new_with_config(skyze_config_source_t *config);
int skyze_source_is_valid(skyze_source_t *source);
void skyze_source_free(skyze_source_t *source);
gboolean create_rtsp_source (skyze_source_t *source, skyze_config_source_t * config);
#endif
