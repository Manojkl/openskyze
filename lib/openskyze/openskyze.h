#ifndef _OPENSKYZE_H
#define _OPENSKYZE_H

#include <glib.h>
#include <gst/gst.h>

#include <openskyze/common.h>
#include <openskyze/config.h>
#include <openskyze/source.h>
#include <openskyze/source_priv.h> // this will be removed
#include <openskyze/sink.h>
#include <openskyze/inference.h>
#include <openskyze/app.h>

#endif
