#ifndef _SKYZE_APP_H
#define _SKYZE_APP_H

#include <openskyze/openskyze.h>

typedef struct _skyze_app_t skyze_app_t;

skyze_app_t *skyze_app_new();
skyze_app_t *skyze_app_new_with_config(const char *config);
int skyze_app_run(skyze_app_t *app);
int skyze_app_source_add(skyze_app_t *app, skyze_source_t *source);
int skyze_app_sink_add(skyze_app_t *app, skyze_sink_t *sink);
int skyze_app_inference_add(skyze_app_t *app, skyze_inference_t *inference);
int skyze_app_message_consumer_add(skyze_app_t *app);
void skyze_app_free(skyze_app_t *app);

#endif
