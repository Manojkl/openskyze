#ifndef _OPENSKYZE_COMMON_H
#define _OPENSKYZE_COMMON_H

#include <gst/gst.h>

enum {
	SKYZE_SUCCESS = 0,
	SKYZE_INVALID_URI = -1,
	SKYZE_NO_MEM = -2,
	SKYZE_INVALID_CONFIG = -3,
	SKYZE_INVALID_ARGS = -4,
	SKYZE_APP_INIT_FAILED = -5,
	SKYZE_CONFIG_FAILED = -6,
	SKYZE_PIPELINE_FAILED = -7,
	SKYZE_REQUEST_PAD_FAILED = -8,
	SKYZE_STATIC_PAD_FAILED = -9,
	SKYZE_LINK_PAD_FAILED = -10,
	SKYZE_BIN_ADD_FAILED = -11,
	SKYZE_INVALID_SOURCE = -12,
};

void skyze_init();
void skyze_deinit();

#define SKYZE_LINK_ELEMENT(elem1, elem2) \
    do { \
      if (!gst_element_link (elem1,elem2)) { \
        GstCaps *src_caps, *sink_caps; \
        src_caps = gst_pad_query_caps ((GstPad *) (elem1)->srcpads->data, NULL); \
        sink_caps = gst_pad_query_caps ((GstPad *) (elem2)->sinkpads->data, NULL); \
        g_print("Failed to link '%s' (%s) and '%s' (%s)", \
            GST_ELEMENT_NAME (elem1), \
            gst_caps_to_string (src_caps), \
            GST_ELEMENT_NAME (elem2), \
            gst_caps_to_string (sink_caps)); \
        goto exit; \
      } \
    } while (0)

#define SKYZE_LINK_ELEMENT_FULL(elem1, elem1_pad_name, elem2, elem2_pad_name) \
  do { \
    GstPad *elem1_pad = gst_element_get_static_pad(elem1, elem1_pad_name); \
    GstPad *elem2_pad = gst_element_get_static_pad(elem2, elem2_pad_name); \
    GstPadLinkReturn ret = gst_pad_link (elem1_pad,elem2_pad); \
    if (ret != GST_PAD_LINK_OK) { \
      gchar *n1 = gst_pad_get_name (elem1_pad); \
      gchar *n2 = gst_pad_get_name (elem2_pad); \
      g_print("Failed to link '%s' and '%s': %d", \
          n1, n2, ret); \
      g_free (n1); \
      g_free (n2); \
      gst_object_unref (elem1_pad); \
      gst_object_unref (elem2_pad); \
      goto exit; \
    } \
    gst_object_unref (elem1_pad); \
    gst_object_unref (elem2_pad); \
  } while (0)

#define SKYZE_BIN_ADD_GHOST_PAD_NAMED(bin, elem, pad, ghost_pad_name) \
  do { \
    GstPad *gstpad = gst_element_get_static_pad (elem, pad); \
    if (!gstpad) { \
      g_print("Could not find '%s' in '%s'", pad, \
          GST_ELEMENT_NAME(elem)); \
      goto exit; \
    } \
    gst_element_add_pad (bin, gst_ghost_pad_new (ghost_pad_name, gstpad)); \
    gst_object_unref (gstpad); \
  } while (0)

#define SKYZE_BIN_ADD_GHOST_PAD(bin, elem, pad) \
      SKYZE_BIN_ADD_GHOST_PAD_NAMED (bin, elem, pad, pad)

#define SKYZE_ELEM_ADD_PROBE(probe_id, elem, pad, probe_func, probe_type, probe_data) \
    do { \
      GstPad *gstpad = gst_element_get_static_pad (elem, pad); \
      if (!gstpad) { \
        g_print("Could not find '%s' in '%s'", pad, \
            GST_ELEMENT_NAME(elem)); \
        goto exit; \
      } \
      probe_id = gst_pad_add_probe(gstpad, (probe_type), probe_func, probe_data, NULL); \
      gst_object_unref (gstpad); \
    } while (0)

#define SKYZE_ELEM_REMOVE_PROBE(probe_id, elem, pad) \
    do { \
      if (probe_id == 0 || !elem) { \
          break; \
      } \
      GstPad *gstpad = gst_element_get_static_pad (elem, pad); \
      if (!gstpad) { \
        g_print("Could not find '%s' in '%s'", pad, \
            GST_ELEMENT_NAME(elem)); \
        break; \
      } \
      gst_pad_remove_probe(gstpad, probe_id); \
      gst_object_unref (gstpad); \
    } while (0)


gboolean link_element_to_tee_src_pad (GstElement * tee, GstElement * sinkelem);
gboolean link_element_to_streammux_sink_pad (GstElement *streammux, GstElement *elem, gint index);
gboolean unlink_element_from_streammux_sink_pad (GstElement *streammux, GstElement *elem);
gboolean link_element_to_demux_src_pad (GstElement *demux, GstElement *elem, guint index);
void str_replace (gchar * str, const gchar * replace, const gchar * replace_with);

#endif
