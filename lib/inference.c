#include <stdlib.h>
#include <linux/limits.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>

#include <gst/gst.h>
#include <gstnvdsinfer.h>

#include <openskyze/openskyze.h>

#define GET_FILE_PATH(path) ((path) + (((path) && strstr ((path), "file://")) ? 7 : 0))

enum
{
    SKYZE_INFERENCE_PLUGIN_ = 0,
    SKYZE_INFERENCE_PLUGIN_SERVER,
};

struct _skyze_inference_t
{
    GstElement *bin;
    GstElement *queue;
    GstElement *nvvidconv;
    GstElement *primary_gie;
    GstPadProbeCallback processing_done_buf_prob;
};

static GstPadProbeReturn processing_done_buf_prob (GstPad * pad, GstPadProbeInfo * info, gpointer u_data);

skyze_inference_t *skyze_inference_new()
{
    skyze_inference_t *inference = NULL;
    int rc = SKYZE_SUCCESS;

    inference = g_try_new0(skyze_inference_t, 1);

    if (inference == NULL) {
        rc = SKYZE_NO_MEM;
        goto exit;
    }
exit:

    if (rc != SKYZE_SUCCESS) {
        skyze_inference_free(inference);
        inference = NULL;
    }
    return inference;
}

GstElement *skyze_inference_get_bin(skyze_inference_t *inference)
{
    if (inference) {
        return inference->bin;
    }
    return NULL;
}

GstPadProbeCallback skyze_inference_get_probe(skyze_inference_t *inference)
{
    if (inference) {
        return inference->processing_done_buf_prob;
    }
    return NULL;
}

void skyze_inference_free(skyze_inference_t *inference) 
{
    if (inference) {
        g_free(inference);
        inference = NULL;
    }
}

static void write_infer_output_to_file(
        GstBuffer *buf,
        NvDsInferNetworkInfo *network_info,  NvDsInferLayerInfo *layers_info,
        guint num_layers, guint batch_size, gpointer user_data)
{
    skyze_config_inference_t *config = (skyze_config_inference_t *) user_data;
    guint i;
    static long unsigned int file_write_frame_num = 0;

    /* "gst_buffer_get_nvstream_meta()" can be called on the GstBuffer to get more
     * information about the buffer.*/

    for (i = 0; i < num_layers; i++) {
        NvDsInferLayerInfo *info = &layers_info[i];
        guint element_size = 0;
        guint j;
        FILE *file;
        gchar file_name[PATH_MAX];
        gchar layer_name[128];

        switch (info->dataType) {
            case FLOAT: element_size = 4; break;
            case HALF: element_size = 2; break;
            case INT32: element_size = 4; break;
            case INT8: element_size = 1; break;
        }

        g_strlcpy (layer_name, info->layerName, 128);
        for (j = 0; layer_name[j] != '\0'; j++) {
            layer_name[j] = (layer_name[j] == '/') ? '_' : layer_name[j];
        }

        g_snprintf (file_name, PATH_MAX,
                "%s/%s_batch%010lu_batchsize%02d.bin",
                config->raw_output_directory, layer_name,
                file_write_frame_num, batch_size);
        file_name[PATH_MAX -  1] = '\0';

        file = fopen (file_name, "w");
        if (!file) {
            g_printerr ("Could not open file '%s' for writing:%s\n",
                    file_name, strerror(errno));
            continue;
        }
        fwrite (info->buffer, element_size, info->inferDims.numElements * batch_size, file);
        fclose (file);
    }
    file_write_frame_num++;
}

skyze_inference_t *skyze_inference_new_with_config(skyze_config_inference_t *config)
{
    skyze_inference_t *inference = NULL;
    int rc = SKYZE_SUCCESS;
    gst_nvinfer_raw_output_generated_callback out_callback;

    if (config == NULL) {
        rc = SKYZE_INVALID_ARGS;
        goto exit;
    }

    inference = skyze_inference_new();

    if (inference == NULL) {
        rc = SKYZE_NO_MEM;
        goto exit;
    }

    out_callback = write_infer_output_to_file;

    inference->bin = gst_bin_new ("primary_gie_bin");

    if (!inference->bin) {
        g_print ("Failed to create 'primary_gie_bin'\n");
        goto exit;
    }

    inference->nvvidconv =
        gst_element_factory_make ("nvvideoconvert", "primary_gie_conv");
    if (!inference->nvvidconv) {
        g_print ("Failed to create 'primary_gie_conv'\n");
        goto exit;
    }

    inference->queue = gst_element_factory_make ("queue", "primary_gie_queue");
    if (!inference->queue) {
        g_print ("Failed to create 'primary_gie_queue'\n");
        goto exit;
    }

    switch (config->plugin_type) {
        case SKYZE_INFERENCE_PLUGIN_:
            inference->primary_gie =
                gst_element_factory_make ("nvinfer", "primary_gie");
            break;
        case SKYZE_INFERENCE_PLUGIN_SERVER:
            inference->primary_gie =
                gst_element_factory_make ("nvinferserver", "primary_gie");
            break;
        default:
            g_print ("Failed to create 'primary_gie' "
                    "on unknown plugin_type\n");
            goto exit;
    }

    if (!inference->primary_gie) {
        g_print ("Failed to create 'primary_gie'\n");
        goto exit;
    }

    g_object_set (G_OBJECT (inference->primary_gie),
            "config-file-path", GET_FILE_PATH (config->config_file),
            "process-mode", 1, NULL);

    //if (config->is_batch_size_set)
    g_object_set (G_OBJECT (inference->primary_gie),
            "batch-size", config->batch_size, NULL);

    //if (config->is_interval_set)
    g_object_set (G_OBJECT (inference->primary_gie),
            "interval", config->interval, NULL);

    //if (config->is_unique_id_set)
    g_object_set (G_OBJECT (inference->primary_gie),
            "unique-id", config->gie_unique_id, NULL);

    //if (config->is_gpu_id_set &&
    if (SKYZE_INFERENCE_PLUGIN_SERVER == config->plugin_type) {
        g_print ("gpu-id: %u in primary-gie group is ignored, "
                "only accept in nvinferserver's config", config->gpu_id);
    }
    //}

    if (config->raw_output_directory) {
        g_object_set (G_OBJECT (inference->primary_gie),
                "raw-output-generated-callback", out_callback,
                "raw-output-generated-userdata", config,
                NULL);
    }

    if (SKYZE_INFERENCE_PLUGIN_ == config->plugin_type) {
        if (config->gpu_id)
            g_object_set (G_OBJECT (inference->primary_gie),
                    "gpu-id", config->gpu_id, NULL);

        if (config->model_file) {
            g_object_set (G_OBJECT (inference->primary_gie), "model-engine-file",
                    GET_FILE_PATH (config->model_file), NULL);
        }
    }

    g_object_set (G_OBJECT (inference->nvvidconv), "gpu-id", config->gpu_id, NULL);
    g_object_set (G_OBJECT (inference->nvvidconv), "nvbuf-memory-type",
            config->nvbuf_memory_type, NULL);

    gst_bin_add_many (GST_BIN (inference->bin), inference->queue,
            inference->nvvidconv, inference->primary_gie, NULL);

    SKYZE_LINK_ELEMENT (inference->queue, inference->nvvidconv);

    SKYZE_LINK_ELEMENT (inference->nvvidconv, inference->primary_gie);

    SKYZE_BIN_ADD_GHOST_PAD (inference->bin, inference->primary_gie, "src");

    SKYZE_BIN_ADD_GHOST_PAD (inference->bin, inference->queue, "sink");

exit:
    if (!rc != SKYZE_SUCCESS) {
        if (inference) {
            skyze_inference_free(inference);
            inference = NULL;
        }
        g_print ("%s failed\n", __func__);
    }
    return inference;
}

/**
 * Buffer probe function to get the results of primary infer.
 * Here it demonstrates the use by dumping bounding box coordinates in
 * kitti format.
 */
static GstPadProbeReturn processing_done_buf_prob (GstPad * pad, GstPadProbeInfo * info, gpointer u_data)
{
  GstBuffer *buf = (GstBuffer *) info->data;
  skyze_app_t *app = (skyze_app_t *) u_data;

  NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
  if (!batch_meta) {
    g_print("Batch meta not found for buffer %p\n", buf);
    return GST_PAD_PROBE_OK;
  }

  //write_kitti_output (appCtx, batch_meta);

  return GST_PAD_PROBE_OK;
}
