#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#define MAX_SOURCE_BINS 10
#include <gstnvdsmeta.h>
#include <gst/rtsp/gstrtsptransport.h>

#include <nvdsgstutils.h>
#include <gst-nvdssr.h>

#include <openskyze/openskyze.h>

#define SRC_CONFIG_KEY "src_config"
#define SOURCE_RESET_INTERVAL_SEC 60

typedef struct {
    char *pad;
} skyze_source_rtsp_priv_t;

static gboolean install_mux_eosmonitor_probe = FALSE;

static void cb_sourcesetup (GstElement * object, GstElement * arg0, gpointer data)
{
	(void)object;
	skyze_source_t *source = (skyze_source_t *) data;
	if (g_object_class_find_property (G_OBJECT_GET_CLASS (arg0), "latency")) {
		g_print ("cb_sourcesetup set %d latency\n", source->latency);
		g_object_set (G_OBJECT (arg0), "latency", source->latency, NULL);
	}
}

/*
 * Function to seek the source stream to start.
 * It is required to play the stream in loop.
 */
static gboolean seek_decode (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;
	gboolean ret = TRUE;

	gst_element_set_state (source->bin, GST_STATE_PAUSED);

	ret = gst_element_seek (source->bin, 1.0, GST_FORMAT_TIME,
			(GstSeekFlags) (GST_SEEK_FLAG_KEY_UNIT | GST_SEEK_FLAG_FLUSH),
			GST_SEEK_TYPE_SET, 0, GST_SEEK_TYPE_NONE, (gint64)GST_CLOCK_TIME_NONE);

	if (!ret)
		GST_WARNING ("Error in seeking pipeline");

	gst_element_set_state (source->bin, GST_STATE_PLAYING);

	return FALSE;
}

/**
 * Probe function to drop certain events to support custom
 * logic of looping of each source stream.
 */
static GstPadProbeReturn restart_stream_buf_prob (GstPad * pad, GstPadProbeInfo * info,
		gpointer u_data)
{
	(void)pad;
	GstEvent *event = GST_EVENT (info->data);
	skyze_source_t *source = (skyze_source_t *) u_data;

	if ((info->type & GST_PAD_PROBE_TYPE_BUFFER)) {
		GST_BUFFER_PTS(GST_BUFFER(info->data)) += source->prev_accumulated_base;
	}
	if ((info->type & GST_PAD_PROBE_TYPE_EVENT_BOTH)) {
		if (GST_EVENT_TYPE (event) == GST_EVENT_EOS) {
			g_timeout_add (1, seek_decode, source);
		}

		if (GST_EVENT_TYPE (event) == GST_EVENT_SEGMENT) {
			GstSegment *segment;

			gst_event_parse_segment (event, (const GstSegment **) &segment);
			segment->base = source->accumulated_base;
			source->prev_accumulated_base = source->accumulated_base;
			source->accumulated_base += segment->stop;
		}
		switch (GST_EVENT_TYPE (event)) {
			case GST_EVENT_EOS:
				/* QOS events from downstream sink elements cause decoder to drop
				 * frames after looping the file since the timestamps reset to 0.
				 * We should drop the QOS events since we have custom logic for
				 * looping individual sources. */
			case GST_EVENT_QOS:
			case GST_EVENT_SEGMENT:
			case GST_EVENT_FLUSH_START:
			case GST_EVENT_FLUSH_STOP:
				return GST_PAD_PROBE_DROP;
			default:
				break;
		}
	}
	return GST_PAD_PROBE_OK;
}


static void decodebin_child_added (GstChildProxy * child_proxy, GObject * object,
		gchar * name, gpointer user_data)
{
	(void)child_proxy;
	skyze_source_t *source = (skyze_source_t *) user_data;
	skyze_config_source_t *config = source->config;
	if (g_strrstr (name, "decodebin") == name) {
		g_signal_connect (G_OBJECT (object), "child-added",
				G_CALLBACK (decodebin_child_added), user_data);
	} else if ((g_strrstr (name, "h264parse") == name) ||
			(g_strrstr (name, "h265parse") == name)) {
		g_object_set (object, "config-interval", -1, NULL);
	} else if (g_strrstr (name, "fakesink") == name) {
		g_object_set (object, "enable-last-sample", FALSE, NULL);
	} else if (g_strrstr (name, "nvcuvid") == name) {
		g_object_set (object, "gpu-id", config->gpu_id, NULL);

		g_object_set (G_OBJECT (object), "cuda-memory-type",
				config->cuda_memory_type, NULL);

		g_object_set (object, "source-id", config->camera_id, NULL);
		g_object_set (object, "num-decode-surfaces", config->num_decode_surfaces,
				NULL);
		if (config->Intra_decode) {
			g_object_set (object, "Intra-decode", config->Intra_decode, NULL);
		}
	} else if (g_strstr_len (name, -1, "omx") == name) {
		if (config->Intra_decode) {
			g_object_set (object, "skip-frames", 2, NULL);
		}
		g_object_set (object, "disable-dvfs", TRUE, NULL);
	} else if (g_strstr_len (name, -1, "nvjpegdec") == name) {
		g_object_set (object, "DeepStream", TRUE, NULL);
	} else if (g_strstr_len (name, -1, "nvv4l2decoder") == name) {
		if (config->Intra_decode) {
			g_object_set (object, "skip-frames", 2, NULL);
		}
#ifdef __aarch64__
		g_object_set (object, "enable-max-performance", TRUE, NULL);
#else
		g_object_set (object, "gpu-id", config->gpu_id, NULL);
		g_object_set (G_OBJECT (object), "cudadec-memtype",
				config->cuda_memory_type, NULL);
#endif
		g_object_set (object, "drop-frame-interval", config->drop_frame_interval, NULL);
		g_object_set (object, "num-extra-surfaces", config->num_extra_surfaces,
				NULL);

		/* Seek only if file is the source. */
		if (config->loop && g_strstr_len(config->uri[0], -1, "file:/") == config->uri[0]) {
			SKYZE_ELEM_ADD_PROBE (source->src_buffer_probe, GST_ELEMENT(object),
					"sink", restart_stream_buf_prob,
					(GstPadProbeType) (GST_PAD_PROBE_TYPE_EVENT_BOTH |
						GST_PAD_PROBE_TYPE_EVENT_FLUSH | GST_PAD_PROBE_TYPE_BUFFER),
					source);
		}
	}
exit:
	return;
}

static void cb_newpad2 (GstElement * decodebin, GstPad * pad, gpointer data)
{
	(void)decodebin;
	GstCaps *caps = gst_pad_query_caps (pad, NULL);
	const GstStructure *str = gst_caps_get_structure (caps, 0);
	const gchar *name = gst_structure_get_name (str);

	if (!strncmp (name, "video", 5)) {
		skyze_source_t *source = (skyze_source_t *) data;
		GstPad *sinkpad = gst_element_get_static_pad (source->cap_filter, "sink");
		if (gst_pad_link (pad, sinkpad) != GST_PAD_LINK_OK) {

			g_print("Failed to link decodebin to pipeline");
		} else {
			skyze_config_source_t *config =
				(skyze_config_source_t *) g_object_get_data (G_OBJECT (source->cap_filter),
						SRC_CONFIG_KEY);

			gst_structure_get_int (str, "width", &config->source_width);
			gst_structure_get_int (str, "height", &config->source_height);
			gst_structure_get_fraction (str, "framerate", &config->source_fps_n,
					&config->source_fps_d);

		}
		gst_object_unref (sinkpad);
	}
	gst_caps_unref (caps);
}


static void cb_newpad3 (GstElement * decodebin, GstPad * pad, gpointer data)
{
	(void)decodebin;
	GstCaps *caps = gst_pad_query_caps (pad, NULL);
	const GstStructure *str = gst_caps_get_structure (caps, 0);
	const gchar *name = gst_structure_get_name (str);

	if (g_strrstr (name, "x-rtp")) {
		skyze_source_t *source = (skyze_source_t *) data;
		GstPad *sinkpad = gst_element_get_static_pad (source->depay, "sink");
		if (gst_pad_link (pad, sinkpad) != GST_PAD_LINK_OK) {

			g_print("Failed to link depay loader to rtsp src");
		}
		gst_object_unref (sinkpad);
	}
	gst_caps_unref (caps);
}

/* Returning FALSE from this callback will make rtspsrc ignore the stream.
 * Ignore audio and add the proper depay element based on codec. */
static gboolean cb_rtspsrc_select_stream (GstElement *rtspsrc, guint num, GstCaps *caps,
		gpointer user_data)
{
	(void)rtspsrc;
	(void)num;
	GstStructure *str = gst_caps_get_structure (caps, 0);
	const gchar *media = gst_structure_get_string (str, "media");
	const gchar *encoding_name = gst_structure_get_string (str, "encoding-name");
	gchar elem_name[50];
	skyze_source_t *source = (skyze_source_t *) user_data;
	gboolean ret = FALSE;

	gboolean is_video = (!g_strcmp0 (media, "video"));

	if (!is_video)
		return FALSE;

	/* Create and add depay element only if it is not created yet. */
	if (!source->depay) {
		g_snprintf (elem_name, sizeof (elem_name), "depay_elem%d", source->bin_id);

		/* Add the proper depay element based on codec. */
		if (!g_strcmp0 (encoding_name, "H264")) {
			source->depay = gst_element_factory_make ("rtph264depay", elem_name);
			g_snprintf (elem_name, sizeof (elem_name), "h264parse_elem%d", source->bin_id);
			source->parser = gst_element_factory_make ("h264parse", elem_name);
		} else if (!g_strcmp0 (encoding_name, "H265")) {
			source->depay = gst_element_factory_make ("rtph265depay", elem_name);
			g_snprintf (elem_name, sizeof (elem_name), "h265parse_elem%d", source->bin_id);
			source->parser = gst_element_factory_make ("h265parse", elem_name);
		} else {
			g_print("%s not supported", encoding_name);
			return FALSE;
		}

		if (!source->depay) {
			g_print("Failed to create '%s'", elem_name);
			return FALSE;
		}

		gst_bin_add_many (GST_BIN (source->bin), source->depay, source->parser, NULL);

		SKYZE_LINK_ELEMENT (source->depay, source->parser);
		SKYZE_LINK_ELEMENT (source->parser, source->tee_rtsp_pre_decode);

		if (!gst_element_sync_state_with_parent (source->depay)) {
			g_print("'%s' failed to sync state with parent", elem_name);
			return FALSE;
		}
		gst_element_sync_state_with_parent (source->parser);
	}
	ret = TRUE;
exit:
	return ret;
}


/**
 * Function called at regular interval to check if SKYZE_SOURCE_RTSP type
 * source in the pipeline is down / disconnected. This function try to
 * reconnect the source by resetting that source pipeline.
 */
static gboolean watch_source_status (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;
	struct timeval current_time;
	gettimeofday (&current_time, NULL);
	static struct timeval last_reset_time_global = {0, 0};
	gdouble time_diff_msec_since_last_reset =
		1000.0 * (current_time.tv_sec - last_reset_time_global.tv_sec) +
		(current_time.tv_usec - last_reset_time_global.tv_usec) / 1000.0;

	if (source->reconfiguring) {
		guint time_since_last_reconnect_sec =
			current_time.tv_sec - source->last_reconnect_time.tv_sec;
		if (time_since_last_reconnect_sec >= SOURCE_RESET_INTERVAL_SEC) {
			if (time_diff_msec_since_last_reset > 3000) {
				last_reset_time_global = current_time;
				// source is still not up, reconfigure it again.
				reset_source_pipeline (source);
			}
		}
	} else {
		gint time_since_last_buf_sec = 0;
		g_mutex_lock (&source->bin_lock);
		if (source->last_buffer_time.tv_sec != 0) {
			time_since_last_buf_sec =
				current_time.tv_sec - source->last_buffer_time.tv_sec;
		}
		g_mutex_unlock (&source->bin_lock);

		// Reset source bin if no buffers are received in the last
		// `rtsp_reconnect_interval_sec` seconds.
		if (source->rtsp_reconnect_interval_sec > 0 &&
				time_since_last_buf_sec >= source->rtsp_reconnect_interval_sec) {
			if (time_diff_msec_since_last_reset > 3000) {
				last_reset_time_global = current_time;

				g_print("No data from source %d since last %u sec. Trying reconnection",
						source->bin_id, time_since_last_buf_sec);
				reset_source_pipeline (source);
			}
		}

	}
	return TRUE;
}

/**
 * Function called at regular interval when source bin is
 * changing state async. This function watches the state of
 * the source bin and sets it to PLAYING if the state of source
 * bin stops at PAUSED when changing state ASYNC.
 */
static gboolean watch_source_async_state_change (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;
	GstState state, pending;
	GstStateChangeReturn ret;

	ret = gst_element_get_state (source->bin, &state, &pending, 0);

	// Bin is still changing state ASYNC. Wait for some more time.
	if (ret == GST_STATE_CHANGE_ASYNC)
		return TRUE;

	// Bin state change failed / failed to get state
	if (ret == GST_STATE_CHANGE_FAILURE) {
		source->async_state_watch_running = FALSE;
		return FALSE;
	}

	// Bin successfully changed state to PLAYING. Stop watching state
	if (state == GST_STATE_PLAYING) {
		source->reconfiguring = FALSE;
		source->async_state_watch_running = FALSE;
		return FALSE;
	}

	// Bin has stopped ASYNC state change but has not gone into
	// PLAYING. Expliclity set state to PLAYING and keep watching
	// state
	gst_element_set_state (source->bin, GST_STATE_PLAYING);

	return TRUE;
}

/**
 * Probe function to monitor data output from rtspsrc.
 */
static GstPadProbeReturn rtspsrc_monitor_probe_func (GstPad * pad, GstPadProbeInfo * info,
		gpointer u_data)
{
	(void)pad;
	skyze_source_t *source = (skyze_source_t *) u_data;
	if (info->type & GST_PAD_PROBE_TYPE_BUFFER) {
		g_mutex_lock(&source->bin_lock);
		gettimeofday (&source->last_buffer_time, NULL);
		g_mutex_unlock(&source->bin_lock);
	}
	return GST_PAD_PROBE_OK;
}

/**
 * Probe function to drop EOS events from nvstreammux when RTSP sources
 * are being used so that app does not quit from EOS in case of RTSP
 * connection errors and tries to reconnect.
 */
static GstPadProbeReturn nvstreammux_eosmonitor_probe_func (GstPad * pad, GstPadProbeInfo * info,
		gpointer u_data)
{
	(void)pad;
	(void)u_data;

	if (info->type & GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM) {
		GstEvent *event = (GstEvent *) info->data;
		if (GST_EVENT_TYPE (event) == GST_EVENT_EOS)
			return GST_PAD_PROBE_DROP;
	}
	return GST_PAD_PROBE_OK;
}

gboolean create_rtsp_source (skyze_source_t *source, skyze_config_source_t * config)
{
	NvDsSRContext *ctx = NULL;
	gboolean ret = FALSE;
	gchar elem_name[50];
	GstCaps *caps = NULL;
	GstCapsFeatures *feature = NULL;

	source->bin = gst_bin_new ("rtsp_source");

	if (!source->bin) {
		g_print("Failed to create element 'rtsp_source'");
		goto exit;
	}

	source->latency = config->latency;
	source->rtsp_reconnect_interval_sec = config->rtsp_reconnect_interval_sec;

	g_snprintf (elem_name, sizeof (elem_name), "src_elem%d", source->bin_id);
	source->src_elem = gst_element_factory_make ("rtspsrc", elem_name);
	if (!source->src_elem) {
		g_print("Failed to create '%s'", elem_name);
		goto exit;
	}

	g_signal_connect (G_OBJECT(source->src_elem), "select-stream",
			G_CALLBACK(cb_rtspsrc_select_stream),
			source);

	g_object_set (G_OBJECT (source->src_elem), "location", config->uri[0], NULL);
	g_object_set (G_OBJECT (source->src_elem), "latency", config->latency, NULL);
	g_object_set (G_OBJECT (source->src_elem), "drop-on-latency", TRUE, NULL);
	configure_source_for_ntp_sync (source->src_elem);

	// 0x4 for TCP and 0x7 for All (UDP/UDP-MCAST/TCP)
	if ((config->select_rtp_protocol == GST_RTSP_LOWER_TRANS_TCP)
			|| (config->select_rtp_protocol == (GST_RTSP_LOWER_TRANS_UDP |
					GST_RTSP_LOWER_TRANS_UDP_MCAST | GST_RTSP_LOWER_TRANS_TCP))) {
		g_object_set (G_OBJECT (source->src_elem), "protocols",
				config->select_rtp_protocol, NULL);
		GST_DEBUG_OBJECT (source->src_elem,
				"RTP Protocol=0x%x (0x4=TCP and 0x7=UDP,TCP,UDPMCAST)----\n",
				config->select_rtp_protocol);
	}
	g_signal_connect (G_OBJECT (source->src_elem), "pad-added",
			G_CALLBACK (cb_newpad3), source);

	g_snprintf (elem_name, sizeof (elem_name), "tee_rtsp_elem%d", source->bin_id);
	source->tee_rtsp_pre_decode = gst_element_factory_make ("tee", elem_name);
	if (!source->tee_rtsp_pre_decode) {
		g_print("Failed to create '%s'", elem_name);
		goto exit;
	}

	g_snprintf (elem_name, sizeof (elem_name), "dec_que%d", source->bin_id);
	source->dec_que = gst_element_factory_make ("queue", elem_name);
	if (!source->dec_que) {
		g_print("Failed to create '%s'", elem_name);
		goto exit;
	}

	if (source->rtsp_reconnect_interval_sec > 0) {
		SKYZE_ELEM_ADD_PROBE (source->rtspsrc_monitor_probe, source->dec_que,
				"sink", rtspsrc_monitor_probe_func,
				GST_PAD_PROBE_TYPE_BUFFER,
				source);
		install_mux_eosmonitor_probe = TRUE;
	}

	g_snprintf (elem_name, sizeof (elem_name), "decodebin_elem%d", source->bin_id);
	source->decodebin = gst_element_factory_make ("decodebin", elem_name);
	if (!source->decodebin) {
		g_print("Failed to create '%s'", elem_name);
		goto exit;
	}

	g_signal_connect (G_OBJECT (source->decodebin), "pad-added",
			G_CALLBACK (cb_newpad2), source);
	g_signal_connect (G_OBJECT (source->decodebin), "child-added",
			G_CALLBACK (decodebin_child_added), source);


	g_snprintf (elem_name, sizeof (elem_name), "src_que%d", source->bin_id);
	source->cap_filter = gst_element_factory_make ("queue", elem_name);
	if (!source->cap_filter) {
		g_print("Failed to create '%s'", elem_name);
		goto exit;
	}

	g_mutex_init (&source->bin_lock);
	g_snprintf(elem_name, sizeof(elem_name), "nvvidconv_elem%d", source->bin_id);
	source->nvvidconv = gst_element_factory_make("nvvideoconvert", elem_name);
	if (!source->nvvidconv)
	{
		g_print("Could not create element 'nvvidconv_elem'");
		goto exit;
	}
	caps = gst_caps_new_empty_simple("video/x-raw");
	feature = gst_caps_features_new("memory:NVMM", NULL);
	gst_caps_set_features(caps, 0, feature);

	source->cap_filter1 =
		gst_element_factory_make("capsfilter", "src_cap_filter_nvvidconv");
	if (!source->cap_filter1)
	{
		g_print("Could not create 'queue'");
		goto exit;
	}

	g_object_set(G_OBJECT(source->cap_filter1), "caps", caps, NULL);
	gst_caps_unref(caps);
	gst_bin_add_many(GST_BIN(source->bin), source->src_elem, source->tee_rtsp_pre_decode,
			source->dec_que, source->decodebin, source->cap_filter, source->nvvidconv, source->cap_filter1, NULL);

	link_element_to_tee_src_pad(source->tee_rtsp_pre_decode, source->dec_que);
	SKYZE_LINK_ELEMENT (source->dec_que, source->decodebin);

	if (ctx)
		link_element_to_tee_src_pad(source->tee_rtsp_pre_decode, ctx->recordbin);

	SKYZE_LINK_ELEMENT (source->cap_filter, source->nvvidconv);
	SKYZE_LINK_ELEMENT (source->nvvidconv, source->cap_filter1);
	SKYZE_BIN_ADD_GHOST_PAD (source->bin, source->cap_filter1, "src");

	ret = TRUE;

	g_timeout_add (1000, watch_source_status, source);

	// Enable local start / stop events in addition to the one
	// received from the server.
exit:

	if (!ret) {
		g_print("%s failed", __func__);
	}
	return ret;
}

gboolean reset_source_pipeline (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;

	GstState state, pending;
	GstStateChangeReturn ret;

	g_mutex_lock(&source->bin_lock);
	gettimeofday (&source->last_buffer_time, NULL);
	gettimeofday (&source->last_reconnect_time, NULL);
	g_mutex_unlock(&source->bin_lock);

	if (gst_element_set_state (source->bin,
				GST_STATE_NULL) == GST_STATE_CHANGE_FAILURE) {
		GST_ERROR_OBJECT (source->bin, "Can't set source bin to NULL");
		return FALSE;
	}

	if (!gst_element_sync_state_with_parent (source->bin)) {
		GST_ERROR_OBJECT (source->bin, "Couldn't sync state with parent");
	}

	ret = gst_element_get_state (source->bin, &state, &pending, 0);

	if (ret == GST_STATE_CHANGE_ASYNC || ret == GST_STATE_CHANGE_NO_PREROLL) {
		if (!source->async_state_watch_running)
			g_timeout_add (20, watch_source_async_state_change, source);
		source->async_state_watch_running = TRUE;
		source->reconfiguring = TRUE;
	} else if (ret == GST_STATE_CHANGE_SUCCESS && state == GST_STATE_PLAYING) {
		source->reconfiguring = FALSE;
	}
	return FALSE;
}

gboolean set_source_to_playing (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;
	if (source->reconfiguring) {
		gst_element_set_state (source->bin, GST_STATE_PLAYING);
		source->reconfiguring = FALSE;
	}
	return FALSE;
}

gpointer reset_encodebin (gpointer data)
{
	skyze_source_t *source = (skyze_source_t *) data;
	g_usleep (10000);

	source->reset_done = TRUE;

	return NULL;
}
